# Code Structure of Application (Personal Documentation)

## Django 
### Classes, Functions (Backend)

##
**Q:** What are permission classes?
- [Django REST framework docu](https://www.django-rest-framework.org/api-guide/permissions/)
  - Used to grant or deny access for different classes of users to different parts of the API.
  - How permissions are determined in general:
    - Permissions in REST framework are always defined as a list of permission classes.
      - Before running the main body of the view each permission in the list is checked.
      - If any permission check fails, an ```exceptions.PermissionDenied``` or ```exceptions.NotAuthenticated``` 
exception will be raised, and the main body of the view will not run.
  - Object level permissions:
    - used to determine if a user should be allowed to act on a particular object, which will typically be a model 
instance.
    - are run by REST framework's generic views when ```.get_object()``` is called
    - If you're writing your own views and want to enforce object level permissions, or if you override the 
```get_object``` method on a generic view, then you'll need to explicitly call the 
```.check_object_permissions(request, obj)``` method on the view at the point at which you've retrieved the object.

##
**Q:** How to set and differentiate between user roles, groups and permissions?
- [Difference between users, permissions, groups and messages](https://django-book.readthedocs.io/en/latest/chapter14.html):
  - Users: People registered with your site
  - Permissions: Binary (yes/no) flags designating whether a user may perform a certain task
  - Groups: A generic way of applying labels and permissions to more than one user
    - [Group model docu (inkl. fields)](https://docs.djangoproject.com/en/1.11/ref/contrib/auth/#django.contrib.auth.models.Group)
    - Groups are expected to be loaded by some competent administrator post-installation
  - Messages: A simple way to queue and display system messages to users
- [Difference between user roles and groups](https://medium.com/djangotube/django-roles-groups-and-permissions-introduction-a54d1070544)
  - use **roles** for showing different HTML pages to users like based on the user category 
  - use **group** for permission purposes
    - Note: group functionality might be relevant for Teams feature @Emilia
- [Programmatically create user groups using data migrations](https://docs.djangoproject.com/en/3.1/topics/migrations/#data-migrations)
  - Custom migrations allow you to manipulate data already in the database or even create new model instances based on 
specific criteria. They are handy if you have to migrate data to a new data type without losing any information. 
  - (+) As they are migrations, they can be stored in your version control history. They can then be pulled down and run 
by anyone with access to the project, which allows the application to be reproducible in different environments. They 
will also be run whenever someone first sets up the project.
  - Migration files in Django are made up of Operations, and the main operation you use for data migrations is RunPython.

##
**Q:** What are querysets?
- A [QuerySet](https://docs.djangoproject.com/en/4.0/ref/models/querysets/#django.db.models.query.QuerySet) represents a 
collection of objects from your database. It can have zero, one or many filters. 
- Filters narrow down the query results based on the given parameters. The two most common ways to refine a QuerySet 
are:
  - filter(**kwargs): Returns a new QuerySet containing objects that match the given lookup parameters.
  - exclude(**kwargs): Returns a new QuerySet containing objects that do not match the given lookup parameters.
- The result of refining a QuerySet is itself a QuerySet, so it’s possible to chain refinements together.
- In SQL terms, a QuerySet equates to a SELECT statement, and a filter is a limiting clause such as WHERE or LIMIT .
- You get a QuerySet by using your model’s [Manager](https://docs.djangoproject.com/en/4.0/topics/db/managers/#django.db.models.Manager). 
  - Each model has at least one Manager, and it’s called [```objects```](https://docs.djangoproject.com/en/4.0/ref/models/class/#django.db.models.Model.objects) by default.
  - Managers are accessible only via model classes, rather than from model instances, to enforce a separation between 
“table-level” operations and “record-level” operations.
  - The Manager is the main source of QuerySets for a model. For example, Blog.objects.all() returns a QuerySet that 
contains all Blog objects in the database.
- The QuerySet returned by all() describes all objects in the database table.

##
**Q:** What are serializers?
- [Django REST framework docu](https://www.django-rest-framework.org/api-guide/serializers/)
- Serializers allow complex data such as querysets and model instances to be converted to native Python datatypes that 
can then be easily rendered into JSON, XML or other content types. 
- Serializers also provide deserialization, allowing parsed data to be converted back into complex types, after first 
validating the incoming data.
- [```ModelSerializer```](https://www.django-rest-framework.org/api-guide/serializers/#modelserializer) class: shortcut 
for creating serializers that deal with model instances and querysets
- [```HyperlinkedModelSerializer```](https://www.django-rest-framework.org/api-guide/serializers/#hyperlinkedmodelserializer) 
class: similar to the ModelSerializer class except that it uses hyperlinks to represent relationships, rather than 
primary keys.
  - By default the serializer will include a url field instead of a primary key field
  - The url field will be represented using a ```HyperlinkedIdentityField``` serializer field, 
  - any relationships on the model will be represented using a ```HyperlinkedRelatedField``` serializer field.
  - When instantiating a ```HyperlinkedModelSerializer``` you must include the current ```request``` in the serializer 
context. Doing so will ensure that the hyperlinks can include an appropriate hostname, so that the resulting 
representation uses fully qualified URLs (i.e. ```http://api.example.com/accounts/1/```) Rather than relative URLs 
(i.e. ```/accounts/1/```) 
  - If you do want to use relative URLs, you should explicitly pass ```{'request': None}``` in the serializer context.
  - There needs to be a way of determining which views should be used for hyperlinking to model instances. By default 
hyperlinks are expected to correspond to a view name that matches the style ```'{model_name}-detail'```, and looks up 
the instance by a ```pk``` keyword argument. 
  - You can override a URL field view name and lookup field by using either, or both of, the ```view_name``` and 
```lookup_field``` options in the ```extra_kwargs``` setting or set the fields on the serializer explicitely

##
**Q:** What does ```get_user_model()``` do?
- [kite Documentation](https://www.kite.com/python/docs/django.contrib.auth.get_user_model)
- Instead of referring to User directly, you should reference the user model using 
```django.contrib.auth.get_user_model()```
- This method will return the currently active User model – the custom User model if one is specified, or User otherwise
- When you define a foreign key or many-to-many relations to the User model, you should specify the custom model using 
the ```django.conf.settings.AUTH_USER_MODEL``` setting

##
**Q:** What are the arguments of ```models.ForeignKey()```?
- [Django Documentation: models.ForeignKey()](https://docs.djangoproject.com/en/4.0/ref/models/fields/#django.db.models.ForeignKey)
- args: 
  - ```to```: class to which the model is related
  - [```on_delete```](https://docs.djangoproject.com/en/4.0/ref/models/fields/#django.db.models.ForeignKey.on_delete): 
When an object referenced by a ForeignKey is deleted, Django will emulate the behavior of the SQL constraint specified 
by the on_delete argument
    - ```CASCADE```: Django emulates the behavior of the SQL constraint ON DELETE CASCADE and also deletes the object 
containing the ForeignKey
  - [```related_name```](https://docs.djangoproject.com/en/4.0/topics/db/queries/#backwards-related-objects) (opt.): 
The name to use for the relation from the related object back to this one
    - Note that you must set this value when defining relations on abstract models
  - ```related_query_name``` (opt.): The name to use for the reverse filter name from the target model. It defaults to 
the value of related_name or default_related_name if set, otherwise it defaults to the name of the model
- to create a recursive relationship – an object that has a many-to-one relationship with itself – use 
```models.ForeignKey('self', on_delete=models.CASCADE)```

## 
**Q:** What is a UserCreationForm vs. a CustomUserChangeForm?

##
**Q:** What fields are included by default in [```AbstractUser``` model](https://docs.djangoproject.com/en/4.0/ref/contrib/auth/#django.contrib.auth.models.User)?
- username
- first_name
- last_name
- email
- password
- groups
- user_permissions
- is_staff
- is_active
- is_superuser
- last_login
- date_joined

##
**Q:** How to handle datetime objects? How to set the current datetime, i.e. for tests?
[Stackoverflow: Naive Datetime Issue in Django](https://stackoverflow.com/questions/18622007/runtimewarning-datetimefield-received-a-naive-datetime)
- in ```settings.py```: if ```USE_TZ = True``` and ```TIME_ZONE = "UTC"```:
  - ```from django.utils import timezone```
  - create datetime objects with: ```timezone.now()```

##
**Q:** How to make default fields of ```AbstractUser``` model mandatory and unique?
- i.e.: ```email = models.EmailField(blank=False, unique=True)```
- username is mandatory and unique by default

##
**Q:** How to add custom fields to admin view of users?
- [Stackoverflow Article](https://stackoverflow.com/questions/48011275/custom-user-model-fields-abstractuser-not-showing-in-django-admin): adjust ```CustomUserAdmin``` in ```users/admin.py```

##
**Q:** What is the difference between TextField and CharField?
- [Stackoverflow Article](https://stackoverflow.com/questions/7354588/whats-the-difference-between-charfield-and-textfield-in-django#:~:text=CharField%20has%20max_length%20of%20255,validation%20to%20the%20TextArea%20widget.)
- CharField has max_length of 255 characters 
- TextField can hold more than 255 characters. 
  - Use TextField when you have a large string as input. It is good to know that when the max_length parameter is passed 
  into a TextField it passes the length validation to the TextArea widget.

##
**Q:** What is the [```django.urls.path```](https://docs.djangoproject.com/en/4.0/ref/urls/) function?
- Returns an element for inclusion in urlpatterns
- ```path(route, view, kwargs=None, name=None)```
  - ```route```: should be a string that contains a URL pattern. The string may contain angle brackets (like <username> 
above) to capture part of the URL and send it as a keyword argument to the view. The angle brackets may include a 
converter specification (like the int part of <int:section>) which limits the characters matched and may also change the 
type of the variable passed to the view. For example, <int:section> matches a string of decimal digits and converts the 
value to an int.
  - ```view```: view function or the result of as_view() for class-based views. It can also be a 
```django.urls.include()```
    - Note for class-based views: Because Django’s URL resolver expects to send the request and associated arguments to 
a callable function, not a class, class-based views have an as_view() class method which returns a function that can be 
called when a request arrives for a URL matching the associated pattern. The function creates an instance of the class, 
calls setup() to initialize its attributes, and then calls its dispatch() method. dispatch looks at the request to 
determine whether it is a GET, POST, etc, and relays the request to a matching method if one is defined, or raises 
HttpResponseNotAllowed if not

### HTML, Javascript (Frontend)

## 
**Q:** What is the difference between span and div elements?
- A div element is used for block-level organization and styling of page elements, whereas a span element is used for 
inline organization and styling.
- 
##
**Q:** What is a custom element?
- [Official Javascript documentation](https://javascript.info/custom-elements):
  - There are two kinds of custom elements:
    - Autonomous custom elements – “all-new” elements, extending the abstract HTMLElement class.
    - Customized built-in elements – extending built-in elements, like a customized button, based on HTMLButtonElement 
etc.
  - custom elements have to be registered on the ```CustomElementRegistry``` using the ```define()``` method
    - in the parameters we specify the element name, and then the class name that defines its functionality: 
```customElements.define('navbar-el', NavBar);```
    - It is now available to use on our page in HTML, referenced by i.e. ```"navbar-el"```
  - methods:
    - ```connectedCallback```: browser calls this method when the element is added to the document
    - ```disconnectedCallback```: browser calls this method when the element is removed from the document

##
**Q:** What is the difference between functions and asynchronous functions?
- [Outsystems Documentation](https://www.outsystems.com/blog/posts/asynchronous-vs-synchronous-programming/#:~:text=In%20synchronous%20operations%20tasks%20are,before%20the%20previous%20one%20finishes.)
- In synchronous operations tasks are performed one at a time and only when one is completed, the following is 
unblocked. In other words, you need to wait for a task to finish to move to the next one. 
- In asynchronous operations, on the other hand, you can move to another task before the previous one finishes.
  - This way, with asynchronous programming you’re able to deal with multiple requests simultaneously, thus completing 
more tasks in a much shorter period of time.   

##
**Q:** How to redirect a user to the previous page?
- [Stackoverflow](https://stackoverflow.com/questions/18518795/how-to-reload-previous-page-with-javascript/18519238):
  - So your best bet to make this work would be to use location.replace(). You can get the previous URL of where you
came from using document.referrer: ```window.location.replace(document.referrer);```

##
**Q:** How to hide the HTML body until it finishes re-loading?
- [Stackoverflow article](https://stackoverflow.com/questions/40768939/hide-body-until-it-finishes-loading)

## Deployment

### Running the app

##
**Q:** When to run makemigrations and migrate? Will the migrations be transferred to env on Gitlab via CI/CD?
- Definition of both scripts: You should think of [migrations](https://docs.djangoproject.com/en/3.2/topics/migrations/) 
as a version control system for your database schema. 
    - ```python manage.py makemigrations <app>```: is responsible for packaging up your model changes into individual 
migration files (generate the SQL commands)- analogous to commits - 
    - ```python manage.py migrate```: is responsible for applying those to your database (execute the SQL commands)
- When to run them:
  - When you add/change model methods, then you don't need to run ```makemigrations``` and ```migrate```. 
  - But whenever you edit your model fields (adding a new one, changing an existing one or altering any of the arguments
it takes) then you should always run migrations
- Migrations in CI/CD environments (i.e. Gitlab):
  - The migration files for each app live in a “migrations” directory inside of that app, and are designed to be 
committed to, and distributed as part of, its codebase. 
  - You should be making them once on your development machine and then running the same migrations on your colleagues’ 
machines, your staging machines, and eventually your production machines -> note: this has to be included i.e. in 
.gitlab-ci.yml
  - Django will make migrations for any change to your models or fields - even options that don’t affect the database - 
as the only way it can reconstruct a field correctly is to have all the changes in the history, and you might need those 
options in some data migrations later on (for example, if you’ve set custom validators).

##
**Q:** How to remove/reset migrations?
- [Docu](https://simpleisbetterthancomplex.com/tutorial/2016/07/26/how-to-reset-migrations.html): remove all migration 
scripts except __init__.py file and re-create migrations
  - Important: remember to backup custom migrations made by ```python manage.py makemigrations --empty <app>``` first!

## 
**Q:** How to reset the Heroku backend database?
- [Step by Step Code](https://gist.github.com/zulhfreelancer/ea140d8ef9292fa9165e)
  - ```heroku pg:reset --confirm secfit-group32-backend```
  - ```heroku run python backend/secfit/manage.py migrate -a secfit-group32-backend```

##
**Q:** How to export the current status of the local sqlite DB (for re-loading of data)?
- open a terminal
- run ```docker ps``` and copy the CONTAINER ID of tdt4242-base_backend
- run ```docker exec [CONTAINER ID] python manage.py dumpdata --indent 2 > <export-file-name>.json``` -> will create 
[export of data](https://coderwall.com/p/mvsoyg/django-dumpdata-and-loaddata)
  - to only extract users: 
```docker exec [CONTAINER ID] python manage.py dumpdata users.user --indent 2 > seed_users.json```
- run ```docker cp [CONTAINER ID]:/code/<export-file-name>.json backend/secfit``` -> will 
[copy data from container to local dir](https://stackoverflow.com/questions/64613524/docker-cp-throws-no-such-containerpath-on-running-instance)
- adjust Dockerfile to load data upon next container run: ```RUN python manage.py loaddata seed_users.json```

## 
**Q:** How can I check the value of sessionStorage and cookies set?
- Chrome
  - [Session Storage](https://developer.chrome.com/docs/devtools/storage/sessionstorage/)
  - [Cookies](https://developer.chrome.com/docs/devtools/storage/cookies/)

### Test-driven development (TDD) / CI/CD

##
**Q:** What should a typical CI/CD pipeline look like?
- [Gitlab Docu Pipeline Architectures](https://docs.gitlab.com/ee/ci/pipelines/pipeline_architectures.html)
- [Medium intro to Gitlab Jobs, Stages, Pipelines](https://medium.com/@ryzmen/gitlab-fast-pipelines-stages-jobs-c51c829b9aa1)

## 
**Q:** What is the general concept of CI/CD? How does this look like on Gitlab?
- References:
  - [Gitlab Docu](https://docs.gitlab.com/ee/ci/introduction/)
- **Continuous Integration**: Each change submitted to an application, even to development branches, is built and 
tested automatically and continuously. These tests ensure the changes pass all tests, guidelines, and code compliance 
standards you established for your application.
- **Continuous Delivery**: Not only is your application built and tested each time a code change is pushed to the 
codebase, the application is also deployed continuously. However, with continuous delivery, you trigger the deployments 
manually.
- **Continuous Deployment**: instead of deploying your application manually, you set it to be deployed automatically. 
Human intervention is not required.

##
**Q:** What are 3rd party tools for testing the frontend (=rendered HTML and the behavior of web pages)? 
- **Selenium and WebdriverIO**
  - References:
    - [Gitlab Docu](https://docs.gitlab.com/ee/ci/examples/end_to_end_testing_webdriverio/)
  - **Selenium**: Selenium is a piece of software that can control web browsers (=JavaScript-based applications), e.g., 
to make them visit a specific URL 
or interact with elements on the page. It can be programmatically controlled from a variety of programming languages.
  - **WebdriverIO**: 
    - functions:
      - ```describe```: group related tests -> useful if, for example, you want to run the same initialization 
commands (using beforeEach) for multiple tests, such as making sure you are logged in.
      - ```it```: defines an individual test
      - ```browser``` (object):  provides most of the WebdriverIO API methods that are the key to steering the 
browser. In this case, we can use browser.url to visit /page-that-does-not-exist to hit our 404 page. We can then use 
browser.getUrl to verify that the current page is indeed at the location we specified. To interact with the page, we 
can simply pass CSS selectors to browser.element to get access to elements on the page and to interact with them - for 
example, to click on the link back to the home page.

##
**Q:** How to run tests on the frontend with Selenium and WebdriverIO?
- [General Gitlab Docu - Selenium/WebdriverIO](https://docs.gitlab.com/ee/ci/examples/end_to_end_testing_webdriverio/#running-locally)
- [Selenium with Docker and Gitlab](https://github.com/esthervogt/python-gitlabci-selenium)
  - very helpful!
- [E2E Testing with separate backend/frontend](https://bierus.medium.com/end-2-end-testing-with-separated-fronted-f2a5dc5be12)
- [Strategies for removal of test data](https://bierus.medium.com/end-2-end-testing-with-separated-fronted-f2a5dc5be12)

##
**Q:** How to launch a selenium chrome standalone browser?
- [Stackoverflow Article](https://stackoverflow.com/questions/45323271/how-to-run-selenium-with-chrome-in-docker)
  - start container: ```docker run -d -p 4444:4444 selenium/standalone-chrome```
  - stop container: ```docker stop <CONTAINER ID>```
  - run: ```pytest```

##
**Q:** What is the difference between pytest.ini and conftest.py?
- pytest.ini: This is the primary pytest configuration file that allows you to change default behavior. 
- conftest.py: This is a local plugin to allow hook functions and fixtures for the directory where the conftest.py file 
exists and all subdirectories. 

##
**Q:** How to make two tests depend on each other?
- [Stackoverflow article](https://stackoverflow.com/questions/10464502/how-can-i-skip-a-test-if-another-test-fails-with-py-test)
  - use [```pip install pytest-dependency```](https://pypi.org/project/pytest-dependency/)

##
**Q:** What are helpful arguments for pytest?
- ```pytest -rP```: shows the captured output of passed tests
- ```pytest -rx```: shows the captured output of failed tests (default behaviour)

## 
**Q:** What criteria is tested by the django EmailValidator?
- [Source Code EmailValidator](max length for domain name labels is 63 characters per RFC 1034)

##
**Q:** How to parameterize the generation of test functions?
- [Stackoverflow Article](https://stackoverflow.com/questions/32999470/parametrize-pytest-fixture/33208377#33208377)

##
**Q:** How to connect to localhost urls when running selenium in standalone container?
- [Stackoverflow Article](https://stackoverflow.com/questions/31324981/how-to-access-host-port-from-docker-container):
use host.docker.internal instead of localhost in the url

##
**Q:** What is the difference between unit, integration, functional, E2E and acceptance tests? 
- References:
  - [Medium Intro Test-driven Development (TDD)](https://medium.com/@koladev/test-driven-development-with-django-unit-testing-integration-testing-with-docker-flask-github-36cedbf1e29b)
  - [High-level comparison unit, integration, functional testing](https://www.softwaretestinghelp.com/the-difference-between-unit-integration-and-functional-testing/#:~:text=Unit%20testing%20means%20testing%20individual,combined%20together%20as%20a%20group.)
- **Unit Tests**:
    - A Unit Test is an *isolated test* that tests *one specific function* without any interaction with dependencies
    - done before Integration testing using *white box testing* techniques
    - does not only check the positive behavior i.e. the correct output in case of valid input, but also the failures 
that occur with invalid input.
    - [Testing Tools](https://www.softwaretestinghelp.com/the-difference-between-unit-integration-and-functional-testing/#:~:text=Unit%20testing%20means%20testing%20individual,combined%20together%20as%20a%20group.): 
JUnit (Java framework), PHPUnit (PHP framework), NUnit (.Net framework) etc. are popular unit testing tools that are 
used for different languages.
    - [Gitlab Docu: Unit test reports](https://docs.gitlab.com/ee/ci/unit_test_reports.html)
- **Integration Testing**:
  - aim of integration testing is to check the functionality, reliability, and performance of the system when integrated
  - form of *black box testing* technique
  - performed on the modules that are unit tested first and then integration testing defines whether the combination of 
  the modules give the desired output or not.
  - can either be done by independent testers or by developers too
  - [Approaches](https://www.softwaretestinghelp.com/the-difference-between-unit-integration-and-functional-testing/#:~:text=Unit%20testing%20means%20testing%20individual,combined%20together%20as%20a%20group.): 
big bang, top down, bottom-up
  - *External integration testing*: Another case will be when you are using an external service for payment and you want 
  to include this service in your test.
- **Functional Testing**:
  - testing the functionality of system based on the requirements
  - form of *black box testing* technique
  - [Approach](https://www.softwaretestinghelp.com/the-difference-between-unit-integration-and-functional-testing/#:~:text=Unit%20testing%20means%20testing%20individual,combined%20together%20as%20a%20group.): 
  Requirement-based, Business scenario-based
  - related to integration tests: however, they signify to the tests that check the entire application’s functionality 
  with all the code running together, nearly a super integration test.
  - Unit testing considers checking a single component of the system whereas functionality testing considers checking 
  the working of an application against the intended functionality described in the *system requirement specification*.
- **End-to-End Testing (E2E)**:
  - testing the system based on end to end user flows -> actions
  - typically performed by a technical QA team
  - For example - Logging into the application, then adding a product to the shopping cart, then going to the check out 
screen and then placing an order and then logging out of the application could be one user flow.
  - can be automated by using a UI testing tool like TestCafe
- **User Acceptance Testing (UAT)**:
  - phase in a typical software development process
  - typically performed by a business user -> visual
  - automated with tools like Jest/TestCafe and concentrate on story functionality and/or what exists on a page if an 
action occurs, i.e. "An authenticated user can view all job applications on the dashboard page."
- **General Testing Best-practice**:
  - Amount of tests per category: code base should have 
    - as many unit tests as possible: easier to write and quicker to execute
    - fewer integration tests 
    - and the least number of functional tests.
  - To attain full coverage, it is required to have unit tests for the paths/lines of code, functional and Integration tests for assurance that the ‘units’ work together cohesively.
  
##
**Q:** How to test Serializers/Deserializers in Django?
- [Django Docu](https://django-testing-docs.readthedocs.io/en/latest/serialization.html)
- [Medium Intro Test-driven Development (TDD)](https://medium.com/@koladev/test-driven-development-with-django-unit-testing-integration-testing-with-docker-flask-github-36cedbf1e29b)
  - includes example of how Serializer Test in Django

## 
**Q:** How to test permissions in Django?
- [Stackoverflow: Unit Test Permissions Django](https://stackoverflow.com/questions/37170911/how-to-unit-test-permissions-in-django-rest-framework)
  
## 
**Q:** How to perform unit and integration tests in Django?
- [django.test.TestCase](https://docs.djangoproject.com/en/4.0/topics/testing/tools/#django.test.TestCase): runs each 
test inside a transaction to provide isolation
- [Django with pytest](https://medium.com/@harshvb7/using-pytest-to-write-tests-in-django-6783674c55b8#:~:text=Pytest%20in%20Django&text=pytest%2Ddjango%20is%20a%20pytest,good%20to%20have%20everything%20covered.)
- [Django with xmlrunner](https://github.com/xmlrunner/unittest-xml-reporting#django-support)
- **Running tests**:
  - ```python manage.py test``` 
    - with warnings enabled: ```python -Wa manage.py test```
    - By default, this will discover test cases (that is, subclasses of unittest.TestCase) in any file named 
    ```test*.py``` under the current working directory.
  - You can specify particular tests to run: i.e. ```manage.py test animals.tests``` (run all the tests in the 
  ```animals.tests``` module)
  - If you press Ctrl-C while the tests are running, the test runner will wait for the currently running test to 
  complete and then exit gracefully. 
    - During a graceful exit the test runner will output details of any test failures, report on how many tests were run 
  and how many errors and failures were encountered, and destroy any test databases as usual.
    - if a test run is forcefully interrupted (pressing Ctrl-C twice), the test database may not be destroyed.
    - As long as your tests are properly isolated, you can run them in parallel to gain a speed up on multi-core 
  hardware. See [test --parallel](https://docs.djangoproject.com/en/4.0/ref/django-admin/#cmdoption-test-parallel).
- **Test Database**:
  - Separate, blank databases are created for the tests.
  - Aside from using a separate database, the test runner will otherwise use all of the same database settings you have 
in your settings file: ENGINE, USER, HOST, etc. The test database is created by the user specified by USER, so you’ll 
need to make sure that the given user account has sufficient privileges to create a new database on the system.
  - Regardless of whether the tests pass or fail, the test databases are destroyed when all the tests have been executed.

## 
**Q:** How to integrate test in Gitlab CI/CD? 
- (1) generate report
  - junit/xml test reports
    - [generate xml reports](https://medium.com/@harshvb7/using-pytest-to-write-tests-in-django-6783674c55b8#:~:text=Pytest%20in%20Django&text=pytest%2Ddjango%20is%20a%20pytest,good%20to%20have%20everything%20covered.) 
    - [generate xml or html reports](https://medium.com/trendyol-tech/reporting-test-results-in-gitlab-6c1938a7472e)
  - coverage reports
    - [generate pytest xml coverage report](https://docs.gitlab.com/ee/user/project/merge_requests/test_coverage_visualization.html#python-example)
- (2) [adjust gitlab-ci.yml accordingly](https://docs.gitlab.com/ee/ci/unit_test_reports.html#python-example)

##
**Q:** What are badges in Gitlab?
- [Gitlab Docu](https://docs.gitlab.com/ee/user/project/badges.html)
  - Badges are a unified way to present condensed pieces of information about your projects. They consist of a small 
image and a URL that the image points to. Examples for badges can be the pipeline status, test coverage, latest release, 
or ways to contact the project maintainers.

##
**Q:** How to get the XPath of an element?
- Right-click "inspect" on the item you are trying to find the XPath.
- Right-click on the highlighted area on the HTML DOM.
- Go to Copy > select 'Copy XPath'.

## SecFit specifics
##
**Q:** What are athlete_files?
- Model ```AthleteFile```: Model for an athlete's file. Contains fields for the athlete for whom this file was uploaded,
    the coach (=owner), and the file itself.
- includes a ```FileField``` with the directory path to upload the file to
- can be i.e. a document or image
##
**Q:** What are coach_files?
- Seems like sth is missing in the implemention since there does not exist an equivalent class for ```AthleteFile``` for 
coaches
##
**Q:** What is the type of the standard user?
- In the implementation provided, the standard user is an athlete and can additionally become a coach to another 
athlete, too.
- Therefore, a user can be both, athlete and coach.
## 
**Q:** How are permissions handled in Secfit App?
- **navbar** design/structure defined in ```frontend/www/scripts/navbar.js```
  - updateNavBar defined in ```frontend/www/scripts/scripts.js``` which includes (de-)activation of single tabs


