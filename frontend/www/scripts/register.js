async function displayRoleGroupList(){
    let response = await sendRequest("GET", `${HOST}/api/role-groups/`);
    let role = document.querySelector("#role");
    if (!response.ok) {
        // handle invalid response
        var options = ['Athlete', 'Coach'];
        // append result options
        for(var j = 0; j < options.length; j++) {
            var opt = options[j];
            var el = document.createElement("option");
            el.textContent = opt;
            el.value = opt;
            role.appendChild(el);
        }

    } else {
        // populate dropdown for role selection
        let options = await response.json();
        let results = options['results'];
        for(var i = 0; i < results.length; i++) {
            var opt = results[i]['name'].replace(/^(role_)/,'');
            var el = document.createElement("option");
            el.textContent = opt;
            el.value = opt;
            role.appendChild(el);
        }
    }
}

async function createNewUser(event) {
    let form = document.querySelector("#form-register-user");
    let formData = new FormData(form);

    // register user -> users/urls.py
    let response = await sendRequest("POST", `${HOST}/api/users/`, formData, "");
    
    if (!response.ok) {
      let data = await response.json();
      let alert = createAlert("Registration failed!", data);
      document.body.prepend(alert);

    } else {
      let body = {
        username:       formData.get("username"),
        password:       formData.get("password"),
        phone_number:   formData.get("phone_number"),
        country:        formData.get("country"),
        city:           formData.get("city"),
        street_address: formData.get("street_address"),
        role:           formData.get("role")
        };
      // forward to user workouts -> workouts/urls.py
      response = await sendRequest("POST", `${HOST}/api/token/`, body);
      if (response.ok) {
          let data = await response.json();
          setCookie("access", data.access, 86400, "/");
          setCookie("refresh", data.refresh, 86400, "/");
          // store user details in session cookies for simplified retrieval and page filtering
          sessionStorage.setItem("username", formData.get("username"));
          sessionStorage.setItem("role", formData.get("role"));
      } else {
        console.log("CAN'T GET JWT TOKEN ON REGISTRATION");
        let data = await response.json();
        let alert = createAlert("Registration could not complete. Try again!", data);
        document.body.prepend(alert);
      }
      form.reset();
      window.location.replace("workouts.html");
    }  
  }


window.addEventListener("DOMContentLoaded", async () => {
    // populate HTML dropdown with js: https://stackoverflow.com/questions/9895082/javascript-populate-drop-down-list-with-array
    await displayRoleGroupList();
});

document.querySelector("#btn-create-account").addEventListener("click", async (event) => await createNewUser(event));
