 //Returns true if it is a Node
function isNode(o){
    return (
      typeof Node === "object" ? o instanceof Node : 
      o && typeof o === "object" && typeof o.nodeType === "number" && typeof o.nodeName==="string"
    );
  }
  
  //Returns true if it is a DOM element    
function isElement(o){
  return (
    typeof HTMLElement === "object" ? o instanceof HTMLElement : //DOM2
    o && typeof o === "object" && o !== null && o.nodeType === 1 && typeof o.nodeName==="string"
);
}


async function displayCurrentRoster() {
    let templateFilledAthlete = document.querySelector("#template-filled-athlete");
    let templateEmptyAthlete = document.querySelector("#template-empty-athlete");
    //let emptyClone = templateEmptyAthlete.content.cloneNode(true);

    let controls = document.querySelector("#controls");

    //let templateFilledTeam = document.querySelector("#template-filled-team");
    //let templateEmptyTeam = document.querySelector("template-empty-team")

    let currentUser = await getCurrentUser();
    for (let athleteUrl of currentUser.athletes) {
        let response = await sendRequest("GET", athleteUrl);
        let athlete = await response.json();

        createFilledRow(templateFilledAthlete, athlete.username, controls, false);
    }
    
    let status = "p";   // pending
    let category = "sent";  
    let response = await sendRequest("GET", `${HOST}/api/offers/?status=${status}&category=${category}`);
    if (!response.ok) {
        let data = await response.json();
        let alert = createAlert("Could not retrieve offers!", data);
        document.body.prepend(alert);
    } else {
        let offers = await response.json();

        for (let offer of offers.results) {
            let response = await sendRequest("GET", offer.recipient);
            let recipient = await response.json();
            createFilledRow(templateFilledAthlete, `${recipient.username} (pending)`, controls, true);
        }
    }

    let emptyClone = templateEmptyAthlete.content.cloneNode(true);
    let emptyDiv = emptyClone.querySelector("div");
    let emptyButton = emptyDiv.querySelector("button");
    


    emptyButton.addEventListener("click", addAthleteRow);
    controls.appendChild(emptyDiv);
    //console.log(typeof(templateEmptyAthlete))
    //console.log(templateEmptyAthlete)

    //console.log(emptyDiv)
    //let emptyCloneTeam = templateEmptyTeam.content.cloneNobe(true);
    //let emptyDivTeam = emptyCloneTeam.querySelector("div");
    //let emptyButton2 = emptyDivTeam.querySelector("button");


    //emptyButton2.addEventListener("click", addTeamRow);
    //controls2.appendChild(emptyDiv);

}
async function displayCurrentRoster2() {
    let controls2 = document.querySelector("#controls2");

    //console.log("1")
    //let templateFilledTeam = document.querySelector("#template-filled-team");
    //let templateEmptyTeam = document.querySelector("#template-empty-team")
    //console.log(typeof(templateEmptyTeam))
    //console.log(templateEmptyTeam)
    //let emptyDivTeam2 = templateEmptyTeam.querySelector("div");
    //console.log(emptyDivTeam2)
    //console.log(isElement(templateEmptyTeam))
    //console.log(isNode(templateEmptyTeam))
    //let emptyCloneTeam = templateEmptyTeam.cloneNode(true);
    //let emptyDivTeam = emptyCloneTeam.querySelector("div");
    //console.log("2")
    //let templateEmptyTeam2 = document.querySelector("template-empty-team")
    //let emptyDivTeam2 = templateEmptyTeam2.querySelector("div");
    //console.log("3")
    let a =  document.querySelector("#template-empty-team")
    // a is the whole template as a fragmentet document.
    //console.log(a)
    let b = a.content.cloneNode(true)
    // b is a clone of a, without the text
    //console.log(b)
    let c = b.querySelector("div")
    // c is the div inside of "template-empty-team"
    //console.log(c)
    let e = c.querySelector("button")
    // e is the button inside of the div, inside of "template-empty-team"
    //console.log(e)

    //let emptyButton2 = emptyDivTeam.querySelector("button");
    //emptyButton2.addEventListener("click", addTeamRow);
    e.addEventListener("click", addTeamRow);
    // here we add an event listener on the button and calls the function addTeamRow
    controls2.appendChild(c);
    // here we add the emptied clone of the template to the div with the id "controls2"
    //console.log("4")
    //controls2.appendChild(emptyDivTeam2)

}

function createFilledRow(templateFilledAthlete, inputValue, controls, disabled) {
    let filledClone = templateFilledAthlete.content.cloneNode(true);
    let filledDiv = filledClone.querySelector("div");
    let filledInput = filledDiv.querySelector("input");
    let filledButton = filledDiv.querySelector("button");
    filledInput.value = inputValue;
    filledInput.disabled = disabled;
    if (!disabled) {
        filledButton.addEventListener("click", removeAthleteRow);
    }
    else {
        filledButton.disabled = true;
    }
    controls.appendChild(filledDiv);
}

async function displayFiles() {
    let user = await getCurrentUser();

    let templateAthlete = document.querySelector("#template-athlete-tab");
    let templateFiles = document.querySelector("#template-files");
    let templateFile = document.querySelector("#template-file");
    let listTab = document.querySelector("#list-tab");
    let navTabContent = document.querySelector("#nav-tabContent");

    for (let fileUrl of user.athlete_files) {
        let response = await sendRequest("GET", fileUrl);
        let file = await response.json();

        response = await sendRequest("GET", file.athlete);
        let athlete = await response.json();

        let tabPanel = document.querySelector(`#tab-contents-${athlete.username}`)
        if (!tabPanel) {
            tabPanel = createTabContents(templateAthlete, athlete, listTab, templateFiles, navTabContent);
        } 

        let divFiles = tabPanel.querySelector(".uploaded-files");
        let aFile = createFileLink(templateFile, file.file);

        divFiles.appendChild(aFile);
    }

    for (let athleteUrl of user.athletes) {
        let response = await sendRequest("GET", athleteUrl);
        let athlete = await response.json();

        let tabPanel = document.querySelector(`#tab-contents-${athlete.username}`)
        if (!tabPanel) {
            tabPanel = createTabContents(templateAthlete, athlete, listTab, templateFiles, navTabContent);
        }
        let uploadBtn = document.querySelector(`#btn-upload-${athlete.username}`);
        uploadBtn.disabled = false;
        uploadBtn.addEventListener("click", async (event) => await uploadFiles(event, athlete));

        let fileInput = tabPanel.querySelector(".form-control");
        fileInput.disabled = false;
    }

    if (user.athlete_files.length == 0 && user.athletes.length == 0) {
        let p = document.createElement("p");
        p.innerText = "There are currently no athletes or uploaded files.";
        document.querySelector("#list-files-div").append(p);
    }
}

// async function displayTeams(){
//     let user = await getCurrentUser();
//     let url = `${HOST}/api/teams-list/`
//     let response = await sendRequest("GET", url)

// }

function createTabContents(templateAthlete, athlete, listTab, templateFiles, navTabContent) {
    let cloneAthlete = templateAthlete.content.cloneNode(true);

    let a = cloneAthlete.querySelector("a");
    a.id = `tab-${athlete.username}`;
    a.href = `#tab-contents-${athlete.username}`;
    a.text = athlete.username;
    listTab.appendChild(a);

    let tabPanel = templateFiles.content.firstElementChild.cloneNode(true);
    tabPanel.id = `tab-contents-${athlete.username}`;

    let uploadBtn = tabPanel.querySelector('input[value="Upload"]');
    uploadBtn.id = `btn-upload-${athlete.username}`;

    navTabContent.appendChild(tabPanel);
    return tabPanel;
}

function createFileLink(templateFile, fileUrl) {
    let cloneFile = templateFile.content.cloneNode(true);
    let aFile = cloneFile.querySelector("a");
    aFile.href = fileUrl;
    let pathArray = fileUrl.split("/");
    aFile.text = pathArray[pathArray.length - 1];
    return aFile;
}

function addAthleteRow(event) {
    let newBlankRow = event.currentTarget.parentElement.cloneNode(true);
    let newInput = newBlankRow.querySelector("input");
    newInput.value = "";
    let controls = document.querySelector("#controls");
    let button = newBlankRow.querySelector("button");
    button.addEventListener("click", addAthleteRow);
    controls.appendChild(newBlankRow);

    event.currentTarget.className = "btn btn-danger btn-remove";
    event.currentTarget.querySelector("i").className = "fas fa-minus";
    event.currentTarget.removeEventListener("click", addAthleteRow);
    event.currentTarget.addEventListener("click", removeAthleteRow);
}

function addTeamRow(event) {
    let newBlankRow = event.currentTarget.parentElement.cloneNode(true);
    let newInput = newBlankRow.querySelector("input");
    newInput.value = "";
    let controls = document.querySelector("#controls2");
    let button = newBlankRow.querySelector("button");
    button.addEventListener("click", addTeamRow);
    controls.appendChild(newBlankRow);

    event.currentTarget.className = "btn btn-danger btn-remove";
    event.currentTarget.querySelector("i").className = "fas fa-minus";
    event.currentTarget.removeEventListener("click", addTeamRow);
    event.currentTarget.addEventListener("click", removeTeamRow);
}


function removeTeamRow(event) {
    event.currentTarget.parentElement.remove();
}

function removeAthleteRow(event) {
    event.currentTarget.parentElement.remove();
}





async function submitRoster() {
    let rosterInputs = document.querySelectorAll('input[name="athlete"]');

    let body = {"athletes": []};
    let currentUser = await getCurrentUser();

    for (let rosterInput of rosterInputs) {
        if (!rosterInput.disabled && rosterInput.value) {
            // get user
            let response = await sendRequest("GET", `${HOST}/api/users/${rosterInput.value}/`);
            if (response.ok) {
                let athlete = await response.json();
                if (athlete.coach == currentUser.url) {
                    body.athletes.push(athlete.id);
                } else {
                    // create offer
                    let body = {'status': 'p', 'recipient': athlete.url};
                    let response = await sendRequest("POST", `${HOST}/api/offers/`, body);
                    if (!response.ok) {
                        let data = await response.json();
                        let alert = createAlert("Could not create offer!", data);
                        document.body.prepend(alert);
                    }
                }
            } else {
                let data = await response.json();
                let alert = createAlert(`Could not retrieve user ${rosterInput.value}!`, data);
                document.body.prepend(alert);
            }
        }
    }
    let response = await sendRequest("PUT", currentUser.url, body);
    location.reload();
}

async function printTeam(){
    let currentUser = await getCurrentUser();
    let username = currentUser.username
    let response = await sendRequest("GET", `${HOST}/api/teams-list/`)
    let templateTeam = document.querySelector("#template-teams")
    let teamList = await response.json();
    let teamContainers = document.querySelector("#all_teams")
    for (let team in teamList){
        // console.log("lol")
        //console.log(teamList[team].coach)
        //console.log(username)
        if (teamList[team].coach == username){
            console.log("het")
            console.log(teamList[team].coach)
            let id = teamList[team].id
            let teamName = teamList[team].name
            let teamCoach = teamList[team].coach
            let teamMembers = teamList[team].members

            let templateClone = templateTeam.content.cloneNode(true)
            let teamTeam = templateClone.querySelector("div");
            let athleteContainer = templateClone.querySelector(".athlete_container")
            let name = teamTeam.querySelector(".name")
            let coach = teamTeam.querySelector(".coach")
            name.innerText = "Team Name & id: " + teamName + "; " + id + "."
            coach.innerText = "Coach: " + teamCoach
            if (teamMembers!=null) {
                athleteContainer.innerText = "My Athletes: "+ teamMembers
            }
            teamContainers.appendChild(teamTeam)
        }
    }
}




// So we write the name of the athlete name_athlete and then we get him from
// User.objects.get(username= name_athlete) , we then add that to the body which we use to update the team objects

async function get_athletes(){
    let url = `${HOST}/api/users/`
    let response = await sendRequest("GET", url)
    let users = await response.json()
    return users
}

async function getCurrenAthletes(id){
    let url = `${HOST}/api/teams-update2/${id}`
    let response = await sendRequest("GET", url)
    let team = await response.json()
    return team.members
}

async function addAthletes(){
    let currentUser = await getCurrentUser();
    let username = currentUser.username
    let athleteOnPage = document.querySelector("#update-athlete")
    athleteOnPage = athleteOnPage.value
    let team = document.querySelector("#update-name")
    team = team.value
    let id = document.querySelector("#update-id")
    id = id.value
    let url = `${HOST}/api/teams-update2/${id}`
    let response2 = await sendRequest("GET", url)
    let team2 = await response2.json()
    let teamMembers=team2.members
    let updateTeam = ""
    if (teamMembers == "" || teamMembers == null) {
        updateTeam = athleteOnPage
    } else {
        updateTeam = teamMembers + ", " + athleteOnPage
    }
    
    body = {
        "name":team,
        "coach":username,
        "members":updateTeam,
    }
    // let currentUser = await getCurrentUser();
    // let userAthletes = currentUser.athletes
    // arr=[]
    // for (let athleteUrl of currentUser.athletes) {
    //     let response = await sendRequest("GET", athleteUrl);
    //     let athlete = await response.json();
    //     arr.append(athlete.username)
    // }
    let response = await sendRequest("PUT", url, body);
    if (response.ok){location.reload();}

}


async function submitRoster2(){
    let rosterInputs1 = document.querySelectorAll('input[name="team"]');
    team_text = rosterInputs1[0].value
    var url = `${HOST}/api/teams-list2/`
    let currentUser = await getCurrentUser();
    let username = currentUser.username
    // let submitForm = new FormData()
    // submitForm.append("name", team_text)
    // submitForm.append("coach", currentUser)
    // submitForm.append("members", "")
    let body_backend_goes={
        "name":team_text,
        "coach":username,
    }

    let response = await sendRequest("POST", url, body_backend_goes);
    //if (response.ok){location.reload();}
    //console.log(body_backend_goes)
    if (response.ok){
        let data = response.json()
        //console.log(data)
    }
    // refresh the page after button was clicked
    location.reload()

}
async function submitRoster3(event){
    //console.log("ffffff")

    let rosterInputs1 = document.querySelectorAll('input[name="team"]');
    team_text = rosterInputs1[0].value

    var url= `${HOST}/api/teams-create/`
    var name = team_text
    fetch(url, {
        method:'POST',
        headers:{
            'Content-type':'application/json',
        },
        body:JSON.stringify({'name':name})
    }
    ).then(function(response){
        submitRoster3
    })
    
}

async function uploadFiles(event, athlete) {
    let form = event.currentTarget.parentElement;
    let inputFormData = new FormData(form);
    let templateFile = document.querySelector("#template-file");

    for (let file of inputFormData.getAll("files")) {
        if (file.size > 0) {
            let submitForm = new FormData();
            submitForm.append("file", file);
            submitForm.append("athlete", athlete.url);

            let response = await sendRequest("POST", `${HOST}/api/athlete-files/`, submitForm, "");
            if (response.ok) {
                let data = await response.json();

                let tabPanel = document.querySelector(`#tab-contents-${athlete.username}`)
                let divFiles = tabPanel.querySelector(".uploaded-files");
                let aFile = createFileLink(templateFile, data["file"]);
                divFiles.appendChild(aFile);
            } else {
                let data = await response.json();
                let alert = createAlert("Could not upload files!", data);
                document.body.prepend(alert);
            }
        }
    }
}

window.addEventListener("DOMContentLoaded", async () => {
    // prevent access to html page if user is either not logged in (aka role is not in session storage) or
    // he doesn't have the correct group/role permissions
    if (("Athlete" == sessionStorage.getItem("role"))|(null == sessionStorage.getItem("role"))){
        // show warning popup and reload the current page
        window.alert("You are not authorized to access this page.");
        window.history.go(-1);
    }
    else {
        document.getElementById("wrapper").style.display = "block";

        await displayCurrentRoster();
        await displayCurrentRoster2();
        await displayFiles();
        await printTeam();
        //submitRoster3();

        let updateTeamButton = document.querySelector("#update-button")
        updateTeamButton.addEventListener("click", async() => await addAthletes());

        let buttonSubmitRoster = document.querySelector("#button-submit-roster");
        buttonSubmitRoster.addEventListener("click", async () => await submitRoster());

        let buttonSubmitRoster2 = document.querySelector("#button-submit-roster-2");
        buttonSubmitRoster2.addEventListener("click", async (event) => await submitRoster2(event));

        let addAthleteBtn = document.querySelector("#update-button")
        addAthleteBtn.addEventListener("click", async () => await addAthletes());


        
        }

});
