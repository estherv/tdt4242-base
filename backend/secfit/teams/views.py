

from rest_framework.decorators import api_view
from rest_framework.response import Response

from teams.models import Teams
from teams.serializers import TeamsSerializer, TeamGetSerializer
from rest_framework import generics, mixins

from rest_framework.parsers import (
    JSONParser,
)
from django.db.models import Q
from rest_framework import filters

class TeamDetail(
    mixins.RetrieveModelMixin,
    mixins.UpdateModelMixin,
    mixins.DestroyModelMixin,
    generics.GenericAPIView,
):
    """Class defining the web response for the details of an individual Meal.

    HTTP methods: GET, PUT, DELETE
    """

    #queryset = Teams.objects.all()
    def get_queryset(self):
        qs = Teams.objects.all()
        return qs
    serializer_class = TeamsSerializer


    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)

    def put(self, request, *args, **kwargs):
        return self.update(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        return self.destroy(request, *args, **kwargs)

class TeamsList(
    mixins.UpdateModelMixin, mixins.ListModelMixin, mixins.CreateModelMixin, generics.GenericAPIView
):
    """Class defining the web response for the creation of a Meal, or displaying a list
    of Meals

    HTTP methods: GET, POST
    """

    serializer_class = TeamsSerializer


    filter_backends = [filters.OrderingFilter]
    ordering_fields = ["name", "coach", "members"]

    def get(self, request, *args, **kwargs):
        #self.serializer_class = TeamGetSerializer

        return self.list(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)

    def put(self, request, *args, **kwargs):
        return self.update(request, *args, **kwargs)


    def perform_create(self, serializer):
        serializer.save()
    
    def get_queryset(self):
        qs = Teams.objects.all()
        return qs




# Create your views here.
#@api_view(['GET'])
# class TeamViewSet(APIView):
#     serializer_class = TeamSerializer
#     queryset = Team.objects.all()

#     def get_queryset(self):
#         return self.queryset.filter(members__in=[self.request.user]).first()
    
#     def perform_create(self, serializer):
#         obj = serializer.save(created_by=self.request.user)
#         obj.members.add(self.request.user)
#         obj.save()

@api_view(['GET'])
def apiOverview(request):
    api_urls = {
        'Teams':'/teams-list',
        'Team_detail':'/teams-detail/<str:pk>',
        'Create':'/teams-create',
        'Update':'/teams-update',
        'Delete':'/teams-delete',
    }

    return Response(api_urls)

@api_view(["GET"])
def teamList(request):
    teams = Teams.objects.all()
    serializer = TeamsSerializer(teams, many=True)
    return Response(serializer.data)

@api_view(["GET"])
def teamDetail(request,pk):
    teams = Teams.objects.get(id=pk)
    serializer = TeamsSerializer(teams, many=False)
    return Response(serializer.data)

@api_view(["POST"])
def teamCreate(request):
    serializer = TeamsSerializer(data=request.data)

    if serializer.is_valid():
        
        serializer.save()
    return Response(serializer.data)

@api_view(["POST"])
def teamUpdate(request, pk):
    team = Teams.objects.get(id=pk)
    serializer = TeamsSerializer(instance=team,data=request.data)
    if serializer.is_valid():
        serializer.save()
    return Response(serializer.data)

@api_view(["DELETE"])
def teamDelete(request, pk):
    team = Teams.objects.get(id=pk)
    team.delete()
    return Response("Item succesfully deleted.")

