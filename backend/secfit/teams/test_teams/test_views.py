from django.test import TestCase
from teams.models import Teams, TeamsFile
from django.urls import reverse
import pytest

pytestmark = pytest.mark.django_db

class TeamListTestCase(TestCase):
   
    def test_call_view_get(self):
        response = self.client.get(reverse("teams-list2"))
        self.assertEqual(response.status_code, 200)
    
    def test_call_view_post(self):
        form_data = {"name": "TeamListTestCase1",
                     "coach": "Coach1",
                     "members": "Member1"}
        response = self.client.post(reverse("teams-list2"), form_data)
        self.assertEqual(response.status_code, 201)
        # print("hello")
        # print(Teams.objects.get(name = "TeamListTestCase1"))
        # print("hello")
        self.assertTrue(Teams.objects.all().filter(coach="Coach1").exists())
        self.assertTrue(Teams.objects.all().filter(members="Member1").exists())