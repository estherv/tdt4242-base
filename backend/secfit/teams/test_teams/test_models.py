from django.test import TestCase
from teams.models import Teams
from django.db.utils import IntegrityError
import pytest

pytestmark = pytest.mark.django_db

class TeamTestCase(TestCase):
    @classmethod
    def setUpTestData(cls):
        Teams.objects.create(name = "test_team_1", coach = "test_coach_1", members="test_member_1" )
    
    def test_name(self):
        self.assertEqual(Teams.objects.get(id=1).name, "test_team_1")

    def test_coach(self):
        self.assertEqual(Teams.objects.get(id=1).coach, "test_coach_1")
    
    def test_members(self):
        self.assertEqual(Teams.objects.get(id=1).members, "test_member_1")
