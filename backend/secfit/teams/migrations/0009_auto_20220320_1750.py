# Generated by Django 3.1 on 2022-03-20 17:50

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('teams', '0008_auto_20220316_0011'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='teams',
            name='members',
        ),
        migrations.AddField(
            model_name='teams',
            name='members',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='members', to=settings.AUTH_USER_MODEL),
        ),
    ]
