from unicodedata import name

from rest_framework import serializers
from users.models import User
from django.contrib.auth import get_user_model, password_validation

from teams.models import Teams, TeamsFile
from users.serializers import UserSerializer, UserGetSerializer


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ("username",)


class TeamGetSerializer(serializers.HyperlinkedModelSerializer):
    members = UserGetSerializer(many=True)

    class Meta:
        model = User
        fields = "__all__"


class TeamsSerializer(serializers.ModelSerializer):
    # members = UserSerializer(many=True, read_only=True)
    class Meta:
        model = Teams
        fields = "__all__"
