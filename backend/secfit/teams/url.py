from django.urls import path
from . import views

urlpatterns = [
    path('', views.apiOverview, name='api-overview'),
    path('teams-list/', views.teamList, name='teams-list'),
#    path('teams-list2/', views.TeamViewSet.as_view(), name='teams-list2'),

    path('teams-detail/<str:pk>', views.teamDetail, name='teams-detail'),
    path('teams-create/', views.teamCreate, name='teams-create'),
    path('teams-list2/', views.TeamsList.as_view(), name='teams-list2'),

    path('teams-update/<str:pk>', views.teamUpdate, name='teams-update'),
    path('teams-update2/<str:pk>', views.TeamDetail.as_view(), name='teams-update'),
    path('teams-delete/<str:pk>', views.teamDelete, name='teams-delete'),
    

]