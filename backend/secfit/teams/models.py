from dis import get_instructions
from django.db import models
from django.contrib.auth import get_user_model
from users.models import User
# Create your models here.




#class Members(models.Model):
#    athletes = models.ForeignKey(User, on_delete=models.CASCADE)

class Teams(models.Model):
    name = models.CharField(max_length=200)
    coach = models.CharField(max_length=200)
    #coach = models.ForeignKey(User, related_name='coach_in_teams', on_delete=models.CASCADE)
    #members = models.ForeignKey(User, related_name='members', blank=True, on_delete=models.CASCADE, null=True)
    members =models.TextField(null=True, blank=True)
    def __str__(self) -> str:
        return self.name

def team_directory_path(instance, filename):
    """Return path for which meal files should be uploaded on the web server

    Args:
        instance (MealFile): MealFile instance
        filename (str): Name of the file

    Returns:
        str: Path where workout file is stored
    """
    return f"teams/{instance.team.id}/{filename}"


class TeamsFile(models.Model):
    team = models.ForeignKey(Teams, on_delete=models.CASCADE, related_name="files")

    file = models.FileField(upload_to=team_directory_path)
