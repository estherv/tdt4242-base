from django.test import RequestFactory, TestCase, Client
from users.models import User
from users.views import UserDetail
from workouts.models import Workout, WorkoutFile
from workouts.views import WorkoutList, WorkoutDetail
from workouts.permissions import (
    IsOwner,
    IsCoachAndVisibleToCoach,
    IsOwnerOfWorkout,
    IsCoachOfWorkoutAndVisibleToCoach,
    IsReadOnly,
    IsPublic,
    IsWorkoutPublic,
)
from django.utils import timezone
from django.urls import reverse
import pytest

pytestmark = pytest.mark.django_db


class TestPermissions(TestCase):

    def setUp(self):
        """
        Set up the Test Data to be used in each of the Testing classes/methods in test_permissions.py
        """
        # create users
        self.test_user_1 = User.objects.create(username='test_user_1',
                                               email='test_user_1@testdomain.com',
                                               password='1234',
                                               role='Athlete')
        self.test_user_2 = User.objects.create(username='test_user_2',
                                               email='test_user_2@testdomain.com',
                                               password='1234',
                                               role='Coach')
        self.test_user_3 = User.objects.create(username='test_user_3',
                                               email='test_user_3@testdomain.com',
                                               password='1234',
                                               role='Athlete')
        # set user relationships
        self.test_user_1.coach = self.test_user_2
        # create a workout and corresponding workout file
        self.tu_1_workout_1 = Workout.objects.create(name='tu_1_workout_1',
                                                     owner=self.test_user_1,
                                                     date=timezone.now())
        self.tu_1_workout_2 = Workout.objects.create(name='tu_1_workout_1',
                                                     owner=self.test_user_1,
                                                     date=timezone.now(),
                                                     visibility='PU')
        self.tu_1_workout_file_1 = WorkoutFile.objects.create(workout=self.tu_1_workout_1,
                                                              owner=self.test_user_1,
                                                              file="")
        self.tu_1_workout_file_2 = WorkoutFile.objects.create(workout=self.tu_1_workout_2,
                                                              owner=self.test_user_1,
                                                              file="")
        # create the request factory object (for sending get/post/etc. requests)
        self.factory = RequestFactory()
        self.client = Client()


class TestIsOwner(TestPermissions):

    def test_has_object_permission(self):
        """
        Test if access will be granted if user is owner of given workout
        """
        request = self.factory.get('/')
        request.user = self.test_user_1
        self.assertTrue(IsOwner().has_object_permission(request=request, obj=self.tu_1_workout_1, view=None))

    def test_has_no_object_permission(self):
        """
        Test if access will be refused if user is not owner of given workout
        """
        request = self.factory.get('/')
        request.user = self.test_user_2
        self.assertFalse(IsOwner().has_object_permission(request=request, obj=self.tu_1_workout_1, view=None))


class TestIsOwnerOfWorkout(TestPermissions):

    def test_has_permission_get(self):
        """
        Test if true will automatically be returned if GET instead of POST request is send (w/o any additional data)
        """
        request = self.factory.get('/')
        self.assertTrue(IsOwnerOfWorkout().has_permission(request=request, view=None))

    def test_has_permission_post(self):
        """
        Test if access will be granted if user is owner of given workout file (with workout as field)
        """
        # send a post request for a workout a user is the owner of
        request = self.factory.post('/')
        request.user = self.test_user_1
        request.data = {'workout': reverse("workout-detail", args=[1]), 'owner': self.test_user_1, 'file': ""}
        self.assertTrue(IsOwnerOfWorkout().has_permission(request=request, view=None))

    def test_has_no_permission_post(self):
        """
        Test if access will be refused if user is not owner of given workout file (with workout as field)
        """
        # send a post request for a workout a user isn't the owner of
        request = self.factory.post('/')
        request.user = self.test_user_2
        request.data = {'workout': reverse("workout-detail", args=[1])}
        self.assertFalse(IsOwnerOfWorkout().has_permission(request=request, view=None))

    def test_has_permission_no_workout(self):
        """
        Test if access will be refused if either no file or no reference to a workout is provided
        """
        # send a post request but w/o workout info
        request = self.factory.post('/')
        request.user = self.test_user_1
        request.data = {}
        self.assertFalse(IsOwnerOfWorkout().has_permission(request=request, view=None))

    def test_has_object_permission(self):
        """
        Test if access will be granted if user is owner of workout file
        """
        request = self.factory.get('/')
        request.user = self.test_user_1
        self.assertTrue(IsOwnerOfWorkout().has_object_permission(request=request, obj=self.tu_1_workout_file_1,
                                                                 view=None))

    def test_has_no_object_permission(self):
        """
        Test if access will be refused if user is not owner of workout file
        """
        request = self.factory.get('/')
        request.user = self.test_user_2
        self.assertFalse(IsOwnerOfWorkout().has_object_permission(request=request, obj=self.tu_1_workout_file_1,
                                                                  view=None))


class TestIsCoachAndVisibleToCoach(TestPermissions):

    def test_has_object_permission(self):
        """
        Test if access will be granted if the user is the coach of the owner of a given workout or workout file
        """
        request = self.factory.get('/')
        # make sure the file is visible to the coach of the file owner
        request.user = self.test_user_2
        self.assertTrue(IsCoachAndVisibleToCoach().has_object_permission(request=request, obj=self.tu_1_workout_file_1,
                                                                         view=None))
        self.assertTrue(IsCoachAndVisibleToCoach().has_object_permission(request=request, obj=self.tu_1_workout_1,
                                                                         view=None))

    def test_has_no_object_permission(self):
        """
        Test if access will be refused if the user is not coach of the owner of a given workout or workout file
        """
        # make sure another athlete can't a given workout file
        request = self.factory.get('/')
        request.user = self.test_user_3
        self.assertFalse(IsCoachAndVisibleToCoach().has_object_permission(request=request, obj=self.tu_1_workout_file_1,
                                                                          view=None))


class TestIsCoachOfWorkoutAndVisibleToCoach(TestPermissions):

    def test_has_object_permission(self):
        """
        Test if access will be refused if the user is not coach of the owner of a given workout or workout file
        """
        request = self.factory.get('/')
        request.user = self.test_user_2
        self.assertTrue(IsCoachOfWorkoutAndVisibleToCoach().has_object_permission(request=request,
                                                                                  obj=self.tu_1_workout_file_1,
                                                                                  view=None))


class TestIsPublic(TestPermissions):

    def test_has_object_permission(self):
        """
        Test if access will be granted if a given object (i.e. workout) has visibility level == 'PU'
        """
        # re-set visibility to be PU = Visible to all authenticated users
        request = self.factory.get('/')
        self.assertTrue(IsPublic().has_object_permission(request=request, obj=self.tu_1_workout_2, view=None))

    def test_has_no_object_permission(self):
        """
        Test if access will be refused if a given object (i.e. workout) has not visibility level == 'PU'
        """
        # check if default visibility set -> Coach
        request = self.factory.get('/')
        self.assertFalse(IsPublic().has_object_permission(request=request, obj=self.tu_1_workout_1, view=None))


class TestIsWorkoutPublic(TestPermissions):

    def test_has_object_permission(self):
        """
        Test if access will be granted if a given object of type workout has visibility level == 'PU'
        """
        request = self.factory.get('/')
        # visibility PU = Visible to all authenticated users
        self.assertTrue(IsWorkoutPublic().has_object_permission(request=request,
                                                                obj=self.tu_1_workout_file_2,
                                                                view=None))

    def test_has_no_object_permission(self):
        """
        Test if access will be refused if a given object of type workout has not visibility level == 'PU'
        """
        # check if default visibility set -> Coach
        request = self.factory.get('/')
        self.assertFalse(IsWorkoutPublic().has_object_permission(request=request,
                                                                 obj=self.tu_1_workout_file_1,
                                                                 view=None))


class TestIsReadOnly(TestPermissions):

    def test_has_object_permission(self):
        """
        Test if access will be granted if a read-only request, i.e. GET is send
        """
        request = self.factory.get('/')
        self.assertTrue(IsReadOnly().has_object_permission(request=request, obj=None, view=None))

    def test_has_no_object_permission(self):
        """
        Test if access will be refused if a write request, i.e. POST is send
        """
        request = self.factory.post('/')
        self.assertFalse(IsReadOnly().has_object_permission(request=request, obj=None, view=None))
