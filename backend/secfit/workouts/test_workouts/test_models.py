from django.test import TestCase
from workouts.models import Workout, WorkoutFile
from users.models import User
import pytest
from django.utils import timezone

pytestmark = pytest.mark.django_db


# Create your tests here.
class WorkoutTestCase(TestCase):

    def setUp(self):
        self.test_user_1 = User.objects.create(username='test_user_1', email='test_user_1@testdomain.com',
                                               password='1234',
                                               role='Athlete')
        self.tu_1_workout_1 = Workout.objects.create(name='tu_1_workout_1',
                                                     owner=self.test_user_1,
                                                     date=timezone.now())

    def test_name(self):
        self.assertEqual(self.tu_1_workout_1.name, "tu_1_workout_1")

    def test_owner(self):
        self.assertEqual(self.tu_1_workout_1.owner, self.test_user_1)


class WorkoutFileTestCase(TestCase):
    def setUp(self):
        WorkoutTestCase.setUp(self)
        self.tu_1_workout_file_1 = WorkoutFile.objects.create(workout=self.tu_1_workout_1, owner=self.test_user_1,
                                                              file="")

    def test_owner(self):
        self.assertEqual(self.tu_1_workout_file_1.owner, self.test_user_1)

    def test_workout(self):
        self.assertEqual(self.tu_1_workout_file_1.workout, self.tu_1_workout_1)
