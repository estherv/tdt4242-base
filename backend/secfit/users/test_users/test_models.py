from django.test import TestCase
from users.models import User, Roles
from django.db.utils import IntegrityError
import pytest

pytestmark = pytest.mark.django_db


# Create your tests here.
class UserTestCase(TestCase):
    @classmethod
    def setUpTestData(cls):
        User.objects.create(username='test_user_1', email='test_user_1@testdomain.com', password='1234', role='Athlete')
        User.objects.create(username='test_user_2', email='test_user_2@testdomain.com', password='1234', role='Coach')

    def test_groups(self):
        # Note: the user will only be added to a user role group AFTER registering on the frontend
        #       aka only by calling the relevant view
        self.assertFalse(User.objects.get(id=1).groups.all().exists())

    def test_username(self):
        # check if correct username was persisted
        self.assertEqual(User.objects.get(id=1).username, "test_user_1")
        self.assertEqual(User.objects.get(id=2).username, "test_user_2")
        # prevent two users registering with same username
        with self.assertRaises(IntegrityError):
            User.objects.create(username='test_user_1',
                                email='other@testdomain.com',
                                password='1234',
                                role='Athlete')

    def test_email(self):
        self.assertEqual(User.objects.get(id=1).email, 'test_user_1@testdomain.com')
        self.assertEqual(User.objects.get(id=2).email, 'test_user_2@testdomain.com')
        # prevent two users registering with same email address
        with self.assertRaises(IntegrityError):
            User.objects.create(username='test_user_2',
                                email='test_user_0@testdomain.com',
                                password='1234',
                                role='Athlete')

    def test_password(self):
        self.assertIsNotNone(User.objects.get(id=1).password)

    def test_role(self):
        self.assertEqual(User.objects.get(id=1).role, 'Athlete')
        self.assertEqual(User.objects.get(id=2).role, 'Coach')
        # prevent creating instances w/o user role -> set "Athlete" as default (i.e. for createsuperuser)
        test_user_3 = User.objects.create(username='test_user_3', email='test_user_3@testdomain.com', password='1234')
        self.assertEqual(test_user_3.role, 'Athlete')
