from django.test import TestCase
from users.models import User, Roles
from django.urls import reverse
import pytest

pytestmark = pytest.mark.django_db

# https://django-testing-docs.readthedocs.io/en/latest/views.html

class RoleGroupListTestCase(TestCase):
    """
    Tests for the view RoleGroupList returning the list of groups for the user roles
    """

    def test_call_view_get(self):
        # check if the view for the user groups can be called
        response = self.client.get(reverse("role-group-list"))
        self.assertEqual(response.status_code, 200)
        # check if user groups are created for all user role selection options
        self.assertListEqual(sorted([i['name'] for i in response.json()['results']]),
                             sorted([f'role_{choice[0]}' for choice in User._meta.get_field('role').choices]))


class UserListTestCase(TestCase):
    """
    Test for UserList
    """

    def test_call_view_get(self):
        response = self.client.get(reverse("user-list"))
        self.assertEqual(response.status_code, 200)

    def test_call_view_post(self):
        valid_form_data = {"username": "UserListTestCase1",
                           "email": "UserListTestCase1@domain.net",
                           "phone_number": "",
                           "country": "",
                           "city": "",
                           "street_address": "",
                           "coach": "",
                           "role": "Athlete",
                           "password": "1234",
                           "password1": "1234"}
        response = self.client.post(reverse("user-list"), valid_form_data)
        # status code "created" == 201
        self.assertEqual(response.status_code, 201)
        # user was added to role user group
        self.assertTrue(User.objects.get(username="UserListTestCase1").groups.filter(name='role_Athlete').exists())

    def test_call_view_post_invalid(self):
        invalid_form_data = {"username": "UserListTestCase2",
                             "email": "UserListTestCase1@domain.net",
                             "phone_number": "",
                             "country": "",
                             "city": "",
                             "street_address": "",
                             "role": "Athlete",
                             "password": "",
                             "password1": ""
                             }
        response = self.client.post(reverse("user-list"), invalid_form_data)
        self.assertEqual(response.status_code, 400)
