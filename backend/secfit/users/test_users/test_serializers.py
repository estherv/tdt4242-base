from django.test import TestCase
from users.serializers import UserSerializer
from rest_framework import serializers
import pytest


pytestmark = pytest.mark.django_db


class UserSerializerTestCase(TestCase):
    """
    Tests for the view UserSerializer
    Note: it is not possible to reach the ValidationError in validate_password(). Post from Blackboard Forum:
    It has come to our attention that there is a method in the UserSerializer class where it is not possible to raise a
    validation error. The method is validate_password. You do not need to cover this method in your task 2.
    But in your final report, you should show that you get full statement coverage on the rest of the class.
    """

    def test_valid_user_serializer(self):
        valid_serializer_data = {
            "email": "testUser1@domain.de",
            "username": "testUser1",
            "password": "1234",
            "password1": "1234",
            "athletes": [],
            "role": "Athlete",
            "coach": None,  # note: this will be set to NULL if no value given
            "workouts": [],
            "coach_files": [],
            "athlete_files": []
        }
        serializer = UserSerializer(data=valid_serializer_data)
        self.assertTrue(serializer.is_valid(raise_exception=True))
        self.assertTrue(serializer.validated_data == valid_serializer_data)

    def test_validate_password(self):
        # test: password blank
        invalid_serializer_data = {"email": "testUser1@domain.de", "username": "testUser1", "athletes": [],
                                   "role": "Athlete", "coach": None, "workouts": [], "coach_files": [],
                                   "phone_number": "", "country": "", "city": "", "street_address": "",
                                   "athlete_files": [], "password": "", "password1": ""}
        serializer1 = UserSerializer(data=invalid_serializer_data)
        with self.assertRaises(serializers.ValidationError):
            serializer1.is_valid(raise_exception=True)
