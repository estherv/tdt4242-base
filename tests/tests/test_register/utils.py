import string
import random
from math import floor


def generate_email(length, charset=string.ascii_letters + string.digits):
    # Django Email Validator: https://github.com/django/django/blob/main/django/core/validators.py#L168-L180
    # randomly choose suffix length
    if length > 6:
        suffix_length = random.randint(2, 3)
    else:
        suffix_length = 2
    suffix = ''.join(random.choice(string.ascii_lowercase) for i in range(suffix_length))
    rem = length - 2 - suffix_length
    # max length for domain name labels is 63 characters per RFC 1034
    domain_length = min(63, int(floor(rem / 2)))
    domain = ''.join(random.choice(charset) for i in range(domain_length))
    prefix = ''.join(random.choice(charset) for i in range(rem - domain_length))
    return prefix + '@' + domain + '.' + suffix


def generate_username(length, charset=string.ascii_letters + string.digits + '_@+.-'):
    return ''.join(random.choice(charset) for i in range(length))


def generate_password(length, charset=string.ascii_letters + string.digits + string.punctuation):
    return ''.join(random.choice(charset) for i in range(length))


def generate_phone_number(length, charset=string.digits):
    return ''.join(random.choice(charset) for i in range(length))


def generate_country(length, charset=string.ascii_letters):
    return ''.join(random.choice(charset) for i in range(length))


def generate_city(length, charset=string.ascii_letters):
    return ''.join(random.choice(charset) for i in range(length))


def generate_street_address(length, charset=string.ascii_letters + string.digits):
    return ''.join(random.choice(charset) for i in range(length))
