import pytest
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from urllib.parse import urljoin
import yaml
from tests.utils import create_account
from tests.test_register.utils import *

def generate_bva_input():
    """
    Perform boundary value analysis (bva) on the registration page: min, min+, nom, max-, max
    -> Input: use boundaries set in test_register_bva.yml
    """
    # load the yaml file containing the bva values
    with open("tests/test_register/test_register_bva.yml") as f:
        bva = yaml.load(f, Loader=yaml.FullLoader)

    # extract the norm value per field + split the duplicate keys
    norms = dict()
    bva_dict = dict()
    for f, b in bva.items():
        if "|" in f:
            for f_sub in f.split("|"):
                bva_dict[f_sub] = list(b.values())
                norms[f_sub] = bva_dict[f_sub][2]
        else:
            bva_dict[f] = list(b.values())
            norms[f] = bva_dict[f][2]

    # loop over the boundary values
    input_lst = []
    idx_dict = {0: 'min-', 1: 'min', 2: 'min+', 3: 'norm', 4: 'max-', 5: 'max', 6: 'max+'}
    for f, b in bva_dict.items():
        input_lengths = norms.copy()
        for i in range(len(b)):
            input_lengths[f] = b[i]
            # note: this additional loop is required to make sure the inputs are unique (i.e. w.r.t. email/username)
            inputs = dict()
            for k, v in input_lengths.items():
                inputs[k] = eval(f'generate_{k}(length={v})')
            # add remark on whether test is expected to pass or fail
            if i in [0, 6]:
                expected_result = f'fail.{idx_dict[i]}.{f}'
            else:
                expected_result = f'pass.{idx_dict[i]}.{f}'
            input_lst.append({expected_result: inputs})

    return input_lst


# note: use fixture to loop over lists of inputs and generate iterative calls to testing function
@pytest.fixture(params=generate_bva_input())
def inputs(request):
    return request.param


def test_registration_boundary_values(inputs, remote_browser, base_url):
    expected_result, bv, var = list(inputs.keys())[0].split('.')
    input_dict = list(inputs.values())[0]
    # check if error was raised in case a failure is expected or passes if this is the expectated result otherwise
    if expected_result == 'fail':
        with pytest.raises(Exception) as e:
            create_account(remote_browser, base_url, input_dict)
        print(f'expected result == "{expected_result}" for {bv} of {var}: {e}')
    else:
        print(f'expected result == "{expected_result}" for {bv} of {var}')
        create_account(remote_browser, base_url, input_dict)
