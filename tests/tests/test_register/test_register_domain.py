import pytest
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from urllib.parse import urljoin
from tests.test_register.utils import *
from itertools import combinations, product
import pandas as pd
import string
from tests.utils import create_account


def pairs(*lists):
    for t in combinations(lists, 2):
        for pair in product(*t):
            yield pair


def generate_ec_to_letter_map():
    # define the equivalence classes
    equivalence_classes = {
       # note: test for uplicate username performed during test case creation
       'username': {'fail': ['', '.', '%'], 'pass': ['B%9a.']},
       # note: the test for duplicate email addresses will be performed implicitely as soon as the
       # same mail address is used multiple times
       'email': {'fail': ['', '@.com'], 'pass': ['name@domain.com']},
       'password': {'fail': ['', '.', '%'], 'pass': ['a']},
       'phone_number': {'fail': ['a', ''], 'pass': [1234567]},
       'country': {'fail': ['999', '', '.', '%'], 'pass': ['Norway']},
       'city': {'fail': ['999', '', '.', '%'], 'pass': ['Milton-Freewater']},
       'street_address': {'fail': ['999', '', '.', '%'], 'pass': ['H9']}
    }

    # generate mapping for simplified generation of input combinations
    expected_outcome_lst = list()
    variable_lst = list(equivalence_classes.keys())
    value_lst = list()
    ec_lst = list()
    for letter_idx, i in enumerate(list(equivalence_classes.values())):
        letter = string.ascii_lowercase[letter_idx]
        ec_lst.append([f'{letter}{ec_idx}' for ec_idx, ec in enumerate(sum(list(i.values()), []))])
        expected_outcome_lst.append(sum([[k] * len(v) for k, v in i.items()], []))
        value_lst.append([ec for ec_idx, ec in enumerate(sum(list(i.values()), []))])
    expected_outcome_dict = dict(zip(sum(ec_lst, []), list(zip(sum(value_lst, []), sum(expected_outcome_lst, [])))))
    return variable_lst, expected_outcome_dict, ec_lst


def generate_sequences(ec_lst):
    # generate the pairs of combinations
    pair_lst = list()
    for pair in eval("pairs(" + ",".join([str(i) for i in ec_lst]) + ")"):
        pair_lst.append(pair)
    # print(f'# combinations: {len(pair_lst)}')

    # combine pairs into min. sequences
    sequences = list()
    pairs_to_cover = pair_lst.copy()
    while len(pairs_to_cover) > 0:
        s1, s2 = pairs_to_cover[0]
        # initialize sequence
        test = [s1, s2]
        # print(f's1, s2: {test}')
        letters_covered = [s1[0], s2[0]]
        # print(f'letters_covered: {letters_covered}')
        # remove s1, s2 from pairs_to_cover
        pairs_to_cover.remove((s1, s2))

        # iteratively add elements to sequence
        while len(letters_covered) < len(ec_lst):
            # check the list of remaining pairs
            next = [(n1, n2) for n1, n2 in pairs_to_cover if
                    ((n1 in test) and not (n2[0] in letters_covered)) or
                    ((n2 in test) and not (n1[0] in letters_covered))]
            # print(f'\tnext (ptc): {next}')

            # if all combinations have been covered already, check the initial list again
            if next == []:
                next = [(n1, n2) for n1, n2 in pair_lst if
                        ((n1 in test) and not (n2[0] in letters_covered)) or
                        ((n2 in test) and not (n1[0] in letters_covered))]
                # print(f'\tnext (pl): {next}')
            else:
                # remove pair from list to cover
                pairs_to_cover.remove(next[0])

            # print(f'next: {next} -> {next[0] if next else ""}')
            # add next pair to list
            test = list(set(test + [next[0][0], next[0][1]]))
            letters_covered = list(set(letters_covered + [next[0][0][0], next[0][1][0]]))
            # print(f'\ttest (len = {len(test)}): {test}')
        # print(f'\ttest (len = {len(test)}): {test}')

        # remove all the pair to pair combinations
        # print(f'\tpairs_to_cover (1) (len={len(pairs_to_cover)}): {pairs_to_cover}')
        for used_pairs in eval("pairs(['" + "'],['".join([i for i in test]) + "'])"):
            if used_pairs in pairs_to_cover:
                pairs_to_cover.remove(used_pairs)
                # print(f'\tadditionally removed: {used_pairs}')
        # print(f'\tpairs_to_cover (2) (len={len(pairs_to_cover)}): {pairs_to_cover}')

        # add the computed sequence to the final sequence
        sequences.append(sorted(test))
        pairs_to_cover = list(sorted(pairs_to_cover))

    return sequences


def map_sequences_back_to_inputs(sequences, mapping, variable_lst):
    ec_seq = []
    for seq in sequences:
        val_to_outcome = [mapping[i] for i in seq]
        ec_seq.append(dict(zip(variable_lst, val_to_outcome)))
    return ec_seq


def generate_inputs():
    variable_lst, expected_outcome_dict, ec_lst = generate_ec_to_letter_map()
    sequences = generate_sequences(ec_lst)
    input_lst = map_sequences_back_to_inputs(sequences, expected_outcome_dict, variable_lst)
    return input_lst


# note: use fixture to loop over lists of inputs and generate iterative calls to testing function
@pytest.fixture(params=generate_inputs())
def inputs(request):
    return request.param


@pytest.fixture(scope="session")
def result_cache():
    return {'username': 0, 'email': 0, 'password': 0, 'phone_number': 0, 'country': 0, 'city': 0, 'street_address': 0}


def test_registration_domain(inputs, remote_browser, base_url, result_cache):
    # assume failure in case at least one attribute is invalid
    if sum([i[1] == 'fail' for i in inputs.values()]) > 0:
        expected_result = 'fail'
    else:
        expected_result = 'pass'
    # reformat input
    input_dict = {k: v[0] for k, v in inputs.items()}
    # check if the same email was already used twice -> adjust in that case!
    for variable in ['email', 'username']:
        if inputs[variable][1] == 'pass':
            if result_cache[variable] >= 2:
                # replace last letter by next letter in alphabet
                next_letter_idx = (string.ascii_lowercase * 3).index('m') + result_cache[variable]
                input_dict[variable] = input_dict[variable][:-1] + (string.ascii_lowercase * 3)[next_letter_idx]
                print(f'{variable} already used {result_cache[variable]} times. '
                      f'New {variable} set: {input_dict[variable]}.')
            # count how many times the same valid (!) username and email were already used
            result_cache[variable] = result_cache[variable] + 1
    # check if error was raised in case a failure is expected or passes if this is the expectated result otherwise
    if expected_result == 'fail':
        with pytest.raises(Exception) as e:
            print(f'expected result == "{expected_result}" for seq = {inputs}')
            create_account(remote_browser, base_url, input_dict)
    else:
        print(f'expected result == "{expected_result}" for seq = {inputs}')
        create_account(remote_browser, base_url, input_dict)


def test_result_cache(result_cache):
    print(result_cache)
