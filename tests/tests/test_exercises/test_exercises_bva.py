import pytest
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from urllib.parse import urljoin
import yaml
import string
import random
from tests.utils import *
from math import ceil, floor


def generate_name(length=10, charset=string.ascii_letters + string.digits):
    return ''.join(random.choice(charset) for i in range(length))


def generate_description(length=10, charset=string.ascii_letters + string.digits):
    return ''.join(random.choice(charset) for i in range(length))


def generate_unit(length, charset=string.ascii_letters):
    return ''.join(random.choice(charset) for i in range(length))


def generate_duration(length=None, charset=None):
    return length


def generate_calories(length=None, charset=None):
    return length


def generate_bva_input():
    """
    Perform boundary value analysis (bva) on the registration page: min, min+, nom, max-, max
    -> Input: use boundaries set in test_register_bva.yml
    """
    # load the yaml file containing the bva values
    with open("tests/test_exercises/test_exercises_bva.yml") as f:
        bva = yaml.load(f, Loader=yaml.FullLoader)

    # extract the norm value per field + split the duplicate keys
    norms = dict()
    bva_dict = dict()
    for f, b in bva.items():
        if "|" in f:
            for f_sub in f.split("|"):
                bva_dict[f_sub] = list(b.values())
                norms[f_sub] = bva_dict[f_sub][2]
        else:
            bva_dict[f] = list(b.values())
            norms[f] = bva_dict[f][2]

    # loop over the boundary values
    input_lst = []
    idx_dict = {0: 'min-', 1: 'min', 2: 'min+', 3: 'norm', 4: 'max-', 5: 'max', 6: 'max+'}
    for f, b in bva_dict.items():
        input_lengths = norms.copy()
        for i in range(len(b)):
            input_lengths[f] = b[i]
            # note: this additional loop is required to make sure the inputs are unique (i.e. w.r.t. email/username)
            inputs = dict()
            for k, v in input_lengths.items():
                inputs[k] = eval(f'generate_{k}(length={v})')
            # add remark on whether test is expected to pass or fail
            if i in [0, 6]:
                expected_result = f'fail.{idx_dict[i]}.{f}'
            else:
                expected_result = f'pass.{idx_dict[i]}.{f}'
            input_lst.append({expected_result: inputs})

    return input_lst


def create_exercise(driver, url, inputs):
    # navigate to registration page
    exercise_url = urljoin(url, 'exercise.html')
    driver.get(exercise_url)
    # get all elements required for form
    content = driver.find_element(by=By.XPATH, value='//*[@id="form-exercise"]')
    # generate defaults for those entries not given in yaml file
    for input in content.find_elements(by=By.CLASS_NAME, value='form-control'):
        field = input.get_attribute('name')
        if field not in inputs.keys():
            driver.find_element(by="name", value=field).send_keys(eval(f'generate_{field}()'))
    # enter the remaining input values
    for k, v in inputs.items():
        driver.find_element(by="name", value=k).send_keys(v)
    print(f'inputs: {inputs}')
    # confirm registration
    click_element(driver, '//*[@id="btn-ok-exercise"]')
    # wait if registration was accepted
    check_redirect(driver, url, next_url='exercises.html')


# note: use fixture to loop over lists of inputs and generate iterative calls to testing function
@pytest.fixture(params=generate_bva_input())
def inputs(request):
    return request.param


# def test_create_test_user(remote_browser, base_url):
#     user_data = {'username': 'testuser1',
#                  'email': 'testuser1@domain.com',
#                  'password': '1234',
#                  'password1': '1234'}
#     create_account(remote_browser, base_url, user_data)


def test_exercises_boundary_values(inputs, remote_browser, base_url):
    login(remote_browser, base_url, 'testuser1', '1234')
    expected_result, bv, var = list(inputs.keys())[0].split('.')
    input_dict = list(inputs.values())[0]
    # check if error was raised in case a failure is expected or passes if this is the expectated result otherwise
    if expected_result == 'fail':
        with pytest.raises(Exception) as e:
            create_exercise(remote_browser, base_url, input_dict)
        print(f'expected result == "{expected_result}" for {bv} of {var}: {e}')
    else:
        print(f'expected result == "{expected_result}" for {bv} of {var}')
        create_exercise(remote_browser, base_url, input_dict)
