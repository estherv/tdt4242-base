import os

from selenium.common.exceptions import NoSuchElementException, TimeoutException
from selenium.webdriver import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from urllib.parse import urljoin
from datetime import datetime
from time import sleep


def login(driver, url, username, password):
    # direct to login page
    driver.get(urljoin(url, 'login.html'))
    # enter user details (testuser1 is included in seed_users.json)
    driver.find_element(by="name", value='username').send_keys(username)
    driver.find_element(by="name", value='password').send_keys(password)
    click_element(driver, '//*[@id="btn-login"]')
    next_url = 'workouts.html'
    WebDriverWait(driver, 5).until(
        lambda driver: driver.current_url == urljoin(url, next_url),
        f'Not forwarded to {next_url}. Invalid inputs?')
    print(f'{username} logged in successfully.')


def logout(driver, url):
    # direct to logout page -> no need to click button anymore!
    driver.get(urljoin(url, 'logout.html'))
    next_url = 'index.html'
    WebDriverWait(driver, 5).until(
        lambda driver: driver.current_url == urljoin(url, next_url),
        f'Not forwarded to {next_url}.')
    print(f'Previous user logged out successfully.')


def click_element(driver, xpath):
    btn = WebDriverWait(driver, 5).until(
        EC.element_to_be_clickable((By.XPATH, xpath)))
    driver.execute_script("arguments[0].click();", btn)


def filter_workout_visibility(workout_dict, visibility):
    return {k: v for k, v in workout_dict.items() if v['visibility'] in visibility}


def filter_workout_owner(workout_dict, owner):
    return {k: v for k, v in workout_dict.items() if k.startswith(owner)}


def filter_workout_coach(workouts, users, username):
    myathletes = [k for k, v in users.items() if v['coach'] == username]
    myathlete_workouts = dict()
    for athlete in myathletes:
        athlete_workouts = filter_workout_owner(workouts, athlete)
        if athlete_workouts:
            myathlete_workouts.update(athlete_workouts)
    return myathlete_workouts


def tab_details(tab_name, is_guest=False):
    if is_guest:
        tab_specs_fix = {
            'all-workouts': {'tab_xpath': '//*[@id="list-all-workouts-list"]',
                             'visibility': {'Public': None}},
            'my-workouts': {'tab_xpath': '//*[@id="list-my-workouts-list"]',
                            'visibility': {}},
            'athlete-workouts': {'tab_xpath': '//*[@id="list-athlete-workouts-list"]',
                                 'visibility': {}, },
            'public-workouts': {'tab_xpath': '//*[@id="list-public-workouts-list"]',
                                'visibility': {'Public': None}, }
        }
    else:
        tab_specs_fix = {
            'all-workouts': {'tab_xpath': '//*[@id="list-all-workouts-list"]',
                             'visibility': {'Public': None, 'Coach': 'coach', 'Private': 'owner'}, },
            'my-workouts': {'tab_xpath': '//*[@id="list-my-workouts-list"]',
                            'visibility': {'Public': 'owner', 'Coach': 'owner', 'Private': 'owner'}},
            'athlete-workouts': {'tab_xpath': '//*[@id="list-athlete-workouts-list"]',
                                 'visibility': {'Public': 'coach', 'Coach': 'coach'}, },
            'public-workouts': {'tab_xpath': '//*[@id="list-public-workouts-list"]',
                                'visibility': {'Public': None}, }
        }
    current_tab = tab_specs_fix[tab_name]
    return current_tab


def generate_workout_dict(workout_dict, users_dict, visibility, username):
    if not visibility:
        return []
    else:
        assert all(
            [i in ['Public', 'Coach', 'Private'] for i in
             visibility.keys()]), f'Incorrect visibility given: {visibility}'
        workouts = dict()
        for v, constraint in visibility.items():
            workout_v = filter_workout_visibility(workout_dict, v)
            if constraint == 'coach':
                workout_v = filter_workout_coach(workout_v, users_dict, username)
            elif constraint == 'owner':
                workout_v = filter_workout_owner(workout_v, username)
            workouts.update(workout_v)
        workout_names = [i['name'] for i in workouts.values()]
        return workout_names


def get_displayed_workouts(driver, url, x_path):
    # make sure user is at workouts page
    driver.get(urljoin(url, 'workouts.html'))
    # get the list of workouts in given tab
    click_element(driver, x_path)
    content = driver.find_element(by=By.XPATH, value='//*[@id="div-content"]')
    workouts_displayed = list()
    workouts_href = list()
    for ele in content.find_elements(by=By.CLASS_NAME, value='mb-1'):
        if ele.get_attribute('class') == 'mb-1':
            if ele.text != '':
                workouts_displayed.append(ele.text)
                parent = ele.find_element(by=By.XPATH, value="..").find_element(by=By.XPATH, value="..")
                workouts_href.append(parent.get_attribute("href"))
    return [i for i in workouts_displayed if i != ''], workouts_href


def validate_displayed_workout_list(driver, url, workouts_dict, users_dict, tab_name, result_cache):
    # get current logged in user from session storage
    username = driver.execute_script("return sessionStorage.getItem('username')")
    # check if username can be found -> None: user is a guest (=logged out)
    if username is None:
        is_guest = True
    else:
        is_guest = False
    # get details of current tab
    current_tab = tab_details(tab_name, is_guest=is_guest)
    # generate list of workouts that should be displayed given the tab specifics
    workouts_retrieved = generate_workout_dict(workouts_dict, users_dict, current_tab['visibility'], username)
    # get the list of workouts that is displayed in the current tab
    workouts_displayed, hrefs = get_displayed_workouts(driver, url, current_tab['tab_xpath'])
    # add the current results to cache
    results = {tab_name: {'tab_xpath': current_tab['tab_xpath'],
                          'workouts': dict(zip([i for i in workouts_displayed if i in workouts_retrieved], hrefs))}}
    result_cache.update(results)
    # compare the list of public workouts created with the list of public workouts shown
    print(f'workouts_displayed: {workouts_displayed}\nworkouts_retrieved: {workouts_retrieved}')
    assert sorted(workouts_retrieved) == sorted(workouts_displayed), f'The workouts displayed ' \
                                                                     f'{workouts_displayed} in tab={tab_name} ' \
                                                                     f'differ from workouts retrieved ' \
                                                                     f'from DB {workouts_retrieved}.'


def get_displayed_workout_data(driver, tab_name, result_cache, type='details'):
    # print(f'result_cache: {result_cache}')
    # initialize dict storing the element lists per type and workout
    workouts = dict()
    # loop over hrefs and follow hrefs
    for workout_name, href in result_cache[tab_name]['workouts'].items():
        # follow link to workout page
        driver.get(href)
        # retrieve the relevant elements per type
        if type == 'details':
            # initialize list storing the workout details
            type_lst = dict()
            # find the input form
            details = driver.find_element(by=By.ID, value='form-workout')
            sleep(2)
            # collect the data from the form controls
            for ele in details.find_elements(by=By.CLASS_NAME, value='form-control'):
                if ele.get_attribute('name') == 'files':
                    # initialize file_dict
                    file_dict = dict()
                    # collect the names and links of the uploaded files
                    for file_form in details.find_elements(by=By.ID, value='uploaded-files'):
                        for file in file_form.find_elements(by=By.CLASS_NAME, value='me-2'):
                            file_dict.update({file_form.text: file.get_attribute('href')})
                    type_lst.update({ele.get_attribute('name'): file_dict})
                else:
                    type_lst.update({ele.get_attribute('name'): ele.get_attribute('value')})

        elif type == 'comments':
            # initialize the list storing the comment details
            type_lst = dict()
            # check the comment section
            comments = driver.find_element(by=By.ID, value='comment-list')
            sleep(2)
            # extract the separate comments
            for idx, ele in enumerate(comments.find_elements(by=By.CLASS_NAME, value='media-body')):
                comment = dict()
                comment.update({'date': date.text for date in ele.find_elements(by=By.CLASS_NAME, value='text-muted')})
                comment.update(
                    {'owner_username': owner.text for owner in
                     ele.find_elements(by=By.CLASS_NAME, value='text-success')})
                comment.update({'content': text.text for text in ele.find_elements(by=By.TAG_NAME, value='p')})
                type_lst.update({f'comment{idx}': comment})

        elif type == 'files':
            # initialize the list storing the file details
            type_lst = dict()
            # get the href of the files to access their subpages
            files = driver.find_element(by=By.ID, value='uploaded-files')
            sleep(2)
            for ele in files.find_elements(by=By.CLASS_NAME, value='me-2'):
                type_lst.update({ele.text: ele.get_attribute('href')})

        else:
            raise AssertionError(f'Invalid type: {type}. Should be either "details", "comments" or "files".')

        # combine all details per workout
        workouts[workout_name] = {'pageref': href, type: type_lst}

    return workouts


def workout_details_per_type(type):
    to_compare = {
        'details': ['name', 'owner_username', 'notes', 'files'],
        'files': [''],
        'comments': ['owner_username', 'content'],
    }
    return to_compare[type]


def compare_dicts(displayed, retrieved, filters):
    # print(f'filters: {filters}')
    different_vals = dict()
    sharedKeys = set(displayed.keys()).intersection(retrieved.keys())
    for key in filters:
        if key in sharedKeys:
            # print(f'{key}: displayed={displayed[key]}, retrieved={retrieved[key]}')
            if key == 'files':
                # get the list of file names
                displayed_file_names = [os.path.split(i)[-1] for i in displayed[key].values()]
                retrieved_file_names = [os.path.split(i)[-1] for i in retrieved[key].values()]
                # compare both lists on whether the contents are identical
                difference = [i for i in list(set(displayed_file_names + displayed_file_names)) if
                              (i not in displayed_file_names) | (i not in retrieved_file_names)]
                if difference:
                    different_vals.update({key: [displayed_file_names, retrieved_file_names]})
            else:
                if displayed[key] != retrieved[key]:
                    different_vals.update({key: [displayed[key], retrieved[key]]})
        else:
            assert ((not displayed[key]) & (key not in retrieved.keys())), f'Detail "{key}" not found in one ' \
                                                                                    f'dataset. '

    return different_vals


def validate_displayed_workout_data(driver, workouts_dict, tab_name, result_cache, type):
    # check the data that is displayed on the details page
    data_displayed = get_displayed_workout_data(driver, tab_name, result_cache, type)
    # check the data that is retrieved for the displayed workouts
    # -> note: this test is only done for those workouts that are displayed since the test for the overall workout
    #    visibility is run prior and will fail if workout is missing
    for workout_name, workout_data in data_displayed.items():
        # get the field names to compare
        filter_keys = workout_details_per_type(type)
        # get the displayed and retrieved details per workout + filter on given keys
        workout_displayed = workout_data[type]
        workout_retrieved = {k: v for k, v in workouts_dict.items() if v['name'] == workout_name}
        workout_retrieved = workout_retrieved[list(workout_retrieved.keys())[0]]
        if (len(workout_displayed) != 0) | (type in workout_retrieved.keys()):
            if (type == 'comments') | (type == 'files'):
                workout_retrieved = workout_retrieved[type]
            print(f'workout_displayed: {workout_displayed}\nworkout_retrieved: {workout_retrieved}')
            if type == 'comments':
                for commentid, comment in workout_retrieved.items():
                    # check if comment found in displayed workouts
                    match = [True for i in workout_displayed.values() if (
                            i['content'] == comment['content']) & (i['owner_username'] == comment['owner_username'])]
                    assert match, f'No matching comment found in display for {workout_name}: {comment}'
                    assert len(match) == 1, f'More than one matching comment found in display for {workout_name}: {comment}'
            elif type == 'files':
                for file_name, file_path in workout_displayed.items():
                    # follow file link
                    driver.get(file_path)
                    WebDriverWait(driver, 5).until(
                        lambda driver: driver.current_url == file_path,
                        f'{workout_name} - Not forwarded to {file_path} (current_url={driver.current_url}).')
            else:
                # compare both dicts
                difference = compare_dicts(workout_displayed, workout_retrieved, filter_keys)
                assert not difference, f'Difference found between displayed and retrieved data for "{type}": {difference}'


def create_account(driver, url, inputs):
    # navigate to registration page
    register_url = urljoin(url, 'register.html')
    driver.get(register_url)
    # enter the input values
    for k, v in inputs.items():
        try:
            driver.find_element(by="name", value=k).send_keys(v)
        except NoSuchElementException as e:
            print(f'No input field for {k} in registration form.')
        if k == 'password':
            driver.find_element(by="name", value=f'password1').send_keys(v)
    print(f'inputs = {inputs}')
    # confirm registration
    click_element(driver, '//*[@id="btn-create-account"]')
    # wait if registration was accepted
    check_redirect(driver, url, next_url='workouts.html')


def create_workout(driver, url, inputs):
    # direct to workout creation page
    driver.get(urljoin(url, 'workout.html'))
    # log new workout
    for k, v in inputs.items():
        if k == 'files':
            for file_name, file_path in v.items():
                total_path = os.path.join(os.getcwd(), file_path)
                driver.find_element(by=By.XPATH, value='//*[@id="customFile"]').send_keys(total_path)
                sleep(2)
        elif k == 'comments':
            print(f'Comments skipped - workout has to be created first.')
        else:
            driver.find_element(by="name", value=k).send_keys(v)
    # insert current date
    datebox = driver.find_element(by=By.XPATH, value='//*[@id="inputDateTime"]')
    datebox.send_keys(datetime.now().strftime('%m%d%Y'))
    datebox.send_keys(Keys.TAB)
    datebox.send_keys(datetime.now().strftime('%H%M%p'))
    # submit
    click_element(driver, '//*[@id="btn-ok-workout"]')
    check_redirect(driver, url, next_url='workouts.html')


def check_redirect(driver, url, next_url='workouts.html'):
    try:
        WebDriverWait(driver, 5).until(
            lambda driver: driver.current_url == urljoin(url, next_url),
            f'Not forwarded to workouts.html (current_url={driver.current_url}). Invalid inputs?')
    except TimeoutException as e:
        # check if alert raised
        alert = driver.find_element(by=By.CSS_SELECTOR,
                                    value='body > div.alert.alert-warning.alert-dismissible.fade.show')
        assert alert, f"{e}: But no alert found."
        raise AssertionError(f'{driver.current_url} - Alert found: \n"{alert.text}"')


def create_comments(driver, url, inputs):
    # navigate to registration page
    register_url = urljoin(url, 'register.html')
