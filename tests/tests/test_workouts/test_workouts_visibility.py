import pytest
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from urllib.parse import urljoin
from time import sleep
from tests.utils import *


class TestWorkoutsAthletes:

    @pytest.fixture(scope='class', autouse=True)
    def setUp(self, setUp, load_users, load_workouts, remote_browser, base_url):
        # get athletes
        users = {k: v for k, v in load_users.items() if k.startswith('athlete')}
        # login athlete
        user_details = users['athlete1']  ##list(self.users.keys())[0]
        login(remote_browser, base_url, user_details['username'], user_details['password'])
        # re-direct user to workouts.html
        remote_browser.get(urljoin(base_url, 'workouts.html'))

    @pytest.fixture(scope="class")
    def result_cache(self):
        return dict()

    # my-workouts

    def test_visibility_my_workouts(self, remote_browser, base_url, load_workouts, load_users, result_cache):
        validate_displayed_workout_list(remote_browser, base_url, load_workouts, load_users, 'my-workouts',
                                        result_cache)

    def test_visibility_my_workouts_details(self, remote_browser, load_workouts, result_cache):
        validate_displayed_workout_data(remote_browser, load_workouts, 'my-workouts', result_cache, type='details')

    def test_visibility_my_workouts_files(self, remote_browser, load_workouts, result_cache):
        validate_displayed_workout_data(remote_browser, load_workouts, 'my-workouts', result_cache, type='files')

    def test_visibility_my_workouts_comments(self, remote_browser, load_workouts, result_cache):
        validate_displayed_workout_data(remote_browser, load_workouts, 'my-workouts', result_cache, type='comments')

    # public-workouts

    def test_visibility_public_workouts(self, remote_browser, base_url, load_workouts, load_users, result_cache):
        validate_displayed_workout_list(remote_browser, base_url, load_workouts, load_users, 'public-workouts',
                                        result_cache)

    def test_visibility_public_workouts_details(self, remote_browser, load_workouts, result_cache):
        validate_displayed_workout_data(remote_browser, load_workouts, 'public-workouts', result_cache, type='details')

    def test_visibility_public_workouts_files(self, remote_browser, load_workouts, result_cache):
        validate_displayed_workout_data(remote_browser, load_workouts, 'public-workouts', result_cache, type='files')

    def test_visibility_public_workouts_comments(self, remote_browser, load_workouts, result_cache):
        validate_displayed_workout_data(remote_browser, load_workouts, 'public-workouts', result_cache, type='comments')

    # athlete-workouts

    def test_visibility_athlete_workouts(self, remote_browser, base_url, load_workouts, load_users, result_cache):
        validate_displayed_workout_list(remote_browser, base_url, load_workouts, load_users, 'athlete-workouts',
                                        result_cache)

    def test_visibility_athlete_workouts_details(self, remote_browser, load_workouts, result_cache):
        validate_displayed_workout_data(remote_browser, load_workouts, 'athlete-workouts', result_cache, type='details')

    def test_visibility_athlete_workouts_files(self, remote_browser, load_workouts, result_cache):
        validate_displayed_workout_data(remote_browser, load_workouts, 'athlete-workouts', result_cache, type='files')

    def test_visibility_athlete_workouts_comments(self, remote_browser, load_workouts, result_cache):
        validate_displayed_workout_data(remote_browser, load_workouts, 'athlete-workouts', result_cache,
                                        type='comments')

    # all-workouts

    def test_visibility_all_workouts(self, remote_browser, base_url, load_workouts, load_users, result_cache):
        validate_displayed_workout_list(remote_browser, base_url, load_workouts, load_users, 'all-workouts',
                                        result_cache)

    def test_visibility_all_workouts_details(self, remote_browser, load_workouts, result_cache):
        validate_displayed_workout_data(remote_browser, load_workouts, 'all-workouts', result_cache, type='details')

    def test_visibility_all_workouts_files(self, remote_browser, load_workouts, result_cache):
        validate_displayed_workout_data(remote_browser, load_workouts, 'all-workouts', result_cache, type='files')

    def test_visibility_all_workouts_comments(self, remote_browser, load_workouts, result_cache):
        validate_displayed_workout_data(remote_browser, load_workouts, 'all-workouts', result_cache, type='comments')


class TestWorkoutsCoach:

    @pytest.fixture(scope='class', autouse=True)
    def setUp(self, setUp, load_users, load_workouts, remote_browser, base_url):
        # get athletes
        users = {k: v for k, v in load_users.items() if k.startswith('coach')}
        # login athlete
        user_details = users['coach1']  ##list(self.users.keys())[0]
        login(remote_browser, base_url, user_details['username'], user_details['password'])

    @pytest.fixture(scope="class")
    def result_cache(self):
        return dict()

    # my-workouts

    def test_visibility_my_workouts(self, remote_browser, base_url, load_workouts, load_users, result_cache):
        validate_displayed_workout_list(remote_browser, base_url, load_workouts, load_users, 'my-workouts',
                                        result_cache)

    def test_visibility_my_workouts_details(self, remote_browser, load_workouts, result_cache):
        validate_displayed_workout_data(remote_browser, load_workouts, 'my-workouts', result_cache, type='details')

    def test_visibility_my_workouts_files(self, remote_browser, load_workouts, result_cache):
        validate_displayed_workout_data(remote_browser, load_workouts, 'my-workouts', result_cache, type='files')

    def test_visibility_my_workouts_comments(self, remote_browser, load_workouts, result_cache):
        validate_displayed_workout_data(remote_browser, load_workouts, 'my-workouts', result_cache, type='comments')

    # public-workouts

    def test_visibility_public_workouts(self, remote_browser, base_url, load_workouts, load_users, result_cache):
        validate_displayed_workout_list(remote_browser, base_url, load_workouts, load_users, 'public-workouts',
                                        result_cache)

    def test_visibility_public_workouts_details(self, remote_browser, load_workouts, result_cache):
        validate_displayed_workout_data(remote_browser, load_workouts, 'public-workouts', result_cache, type='details')

    def test_visibility_public_workouts_files(self, remote_browser, load_workouts, result_cache):
        validate_displayed_workout_data(remote_browser, load_workouts, 'public-workouts', result_cache, type='files')

    def test_visibility_public_workouts_comments(self, remote_browser, load_workouts, result_cache):
        validate_displayed_workout_data(remote_browser, load_workouts, 'public-workouts', result_cache, type='comments')

    # athlete-workouts

    def test_visibility_athlete_workouts(self, remote_browser, base_url, load_workouts, load_users, result_cache):
        validate_displayed_workout_list(remote_browser, base_url, load_workouts, load_users, 'athlete-workouts',
                                        result_cache)

    def test_visibility_athlete_workouts_details(self, remote_browser, load_workouts, result_cache):
        validate_displayed_workout_data(remote_browser, load_workouts, 'athlete-workouts', result_cache, type='details')

    def test_visibility_athlete_workouts_files(self, remote_browser, load_workouts, result_cache):
        validate_displayed_workout_data(remote_browser, load_workouts, 'athlete-workouts', result_cache, type='files')

    def test_visibility_athlete_workouts_comments(self, remote_browser, load_workouts, result_cache):
        validate_displayed_workout_data(remote_browser, load_workouts, 'athlete-workouts', result_cache,
                                        type='comments')

    # all-workouts

    def test_visibility_all_workouts(self, remote_browser, base_url, load_workouts, load_users, result_cache):
        validate_displayed_workout_list(remote_browser, base_url, load_workouts, load_users, 'all-workouts',
                                        result_cache)

    def test_visibility_all_workouts_details(self, remote_browser, load_workouts, result_cache):
        validate_displayed_workout_data(remote_browser, load_workouts, 'all-workouts', result_cache, type='details')

    def test_visibility_all_workouts_files(self, remote_browser, load_workouts, result_cache):
        validate_displayed_workout_data(remote_browser, load_workouts, 'all-workouts', result_cache, type='files')

    def test_visibility_all_workouts_comments(self, remote_browser, load_workouts, result_cache):
        validate_displayed_workout_data(remote_browser, load_workouts, 'all-workouts', result_cache, type='comments')


class TestWorkoutsGuest:

    @pytest.fixture(scope='class', autouse=True)
    def setUp(self, setUp, load_users, load_workouts, remote_browser, base_url):
        # make sure current user is logged out
        logout(remote_browser, base_url)
        # re-direct user to workouts.html
        remote_browser.get(urljoin(base_url, 'workouts.html'))

    @pytest.fixture(scope="class")
    def result_cache(self):
        return dict()

    # my-workouts

    def test_visibility_my_workouts(self, remote_browser, base_url, load_workouts, load_users, result_cache):
        validate_displayed_workout_list(remote_browser, base_url, load_workouts, load_users, 'my-workouts',
                                        result_cache)

    def test_visibility_my_workouts_details(self, remote_browser, load_workouts, result_cache):
        validate_displayed_workout_data(remote_browser, load_workouts, 'my-workouts', result_cache, type='details')

    def test_visibility_my_workouts_files(self, remote_browser, load_workouts, result_cache):
        validate_displayed_workout_data(remote_browser, load_workouts, 'my-workouts', result_cache, type='files')

    def test_visibility_my_workouts_comments(self, remote_browser, load_workouts, result_cache):
        validate_displayed_workout_data(remote_browser, load_workouts, 'my-workouts', result_cache, type='comments')

    # public-workouts

    def test_visibility_public_workouts(self, remote_browser, base_url, load_workouts, load_users, result_cache):
        validate_displayed_workout_list(remote_browser, base_url, load_workouts, load_users, 'public-workouts',
                                        result_cache)

    def test_visibility_public_workouts_details(self, remote_browser, load_workouts, result_cache):
        validate_displayed_workout_data(remote_browser, load_workouts, 'public-workouts', result_cache, type='details')

    def test_visibility_public_workouts_files(self, remote_browser, load_workouts, result_cache):
        validate_displayed_workout_data(remote_browser, load_workouts, 'public-workouts', result_cache, type='files')

    def test_visibility_public_workouts_comments(self, remote_browser, load_workouts, result_cache):
        validate_displayed_workout_data(remote_browser, load_workouts, 'public-workouts', result_cache, type='comments')

    # athlete-workouts

    def test_visibility_athlete_workouts(self, remote_browser, base_url, load_workouts, load_users, result_cache):
        validate_displayed_workout_list(remote_browser, base_url, load_workouts, load_users, 'athlete-workouts',
                                        result_cache)

    def test_visibility_athlete_workouts_details(self, remote_browser, load_workouts, result_cache):
        validate_displayed_workout_data(remote_browser, load_workouts, 'athlete-workouts', result_cache, type='details')

    def test_visibility_athlete_workouts_files(self, remote_browser, load_workouts, result_cache):
        validate_displayed_workout_data(remote_browser, load_workouts, 'athlete-workouts', result_cache, type='files')

    def test_visibility_athlete_workouts_comments(self, remote_browser, load_workouts, result_cache):
        validate_displayed_workout_data(remote_browser, load_workouts, 'athlete-workouts', result_cache,
                                        type='comments')

    # all-workouts

    def test_visibility_all_workouts(self, remote_browser, base_url, load_workouts, load_users, result_cache):
        validate_displayed_workout_list(remote_browser, base_url, load_workouts, load_users, 'all-workouts',
                                        result_cache)

    def test_visibility_all_workouts_details(self, remote_browser, load_workouts, result_cache):
        validate_displayed_workout_data(remote_browser, load_workouts, 'all-workouts', result_cache, type='details')

    def test_visibility_all_workouts_files(self, remote_browser, load_workouts, result_cache):
        validate_displayed_workout_data(remote_browser, load_workouts, 'all-workouts', result_cache, type='files')

    def test_visibility_all_workouts_comments(self, remote_browser, load_workouts, result_cache):
        validate_displayed_workout_data(remote_browser, load_workouts, 'all-workouts', result_cache, type='comments')
