import pytest
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from urllib.parse import urljoin
from tests.utils import login, create_account, click_element
from unittest import TestCase


class TestMyAthletes(TestCase):

    def test_login(self, remote_browser, base_url):
        # direct to login page
        login_url = urljoin(base_url, 'login.html')
        remote_browser.get(login_url)
        # enter user details (testuser1 is included in seed_users.json)
        username, password = 'testuser2', '1234'
        remote_browser.find_element(by="name", value='username').send_keys(username)
        remote_browser.find_element(by="name", value='password').send_keys(password)
        click_element(remote_browser, '//*[@id="btn-login"]')
        next_url = 'workouts.html'
        WebDriverWait(remote_browser, 5).until(
            lambda driver: driver.current_url == urljoin(base_url, next_url),
            f'Not forwarded to {next_url}. Invalid inputs?')


