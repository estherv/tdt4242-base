import pytest
from selenium import webdriver
from selenium.webdriver import Remote, DesiredCapabilities
from tests.utils import *
import yaml


def pytest_addoption(parser):
    """ Parse pytest --option variables from shell """
    parser.addoption('--browser', help='Which test browser?', default='chrome')
    parser.addoption('--local', help='local or CI?', choices=['true', 'false'], default='true')
    parser.addoption('--perform_setup', help='w/ or w/o setup?', choices=['true', 'false'], default='true')


@pytest.fixture(scope='session')
def test_browser(request):
    """ :returns Browser.NAME from --browser option """
    return request.config.getoption('--browser')


@pytest.fixture(scope='session')
def local(request):
    """ :returns true or false from --local option """
    return request.config.getoption('--local')


@pytest.fixture(scope='session')
def perform_setup(request):
    """ :returns true or false from --perform_setup option """
    return request.config.getoption('--perform_setup')


@pytest.fixture(scope='session')
def load_users():
    # load the yaml file containing the user details
    with open("tests/data/user.yml") as f:
        users = yaml.load(f, Loader=yaml.FullLoader)
    return users


@pytest.fixture(scope='session')
def load_workouts():
    # load the yaml file containing the user details
    with open("tests/data/workouts.yml") as f:
        workouts = yaml.load(f, Loader=yaml.FullLoader)
    return workouts


@pytest.fixture(scope='session', autouse=True)
def setUp(perform_setup, load_users, load_workouts, remote_browser, base_url):
    """
    Note: this setup will only be called once per session
    """
    # create the users
    if perform_setup == 'true':
        for userid, user_data in load_users.items():
            create_account(remote_browser, base_url, user_data)
            print(f'User Account for "{userid}" created.')
            # create corresponding workouts
            user_workouts = {k: v for k, v in load_workouts.items() if k.startswith(user_data['username'])}
            for workoutid, workout_data in user_workouts.items():
                create_workout(remote_browser, base_url, workout_data)
                print(f'Workout "{workoutid}" created.')
        # create comments on workouts
        # retrieve all comments
        workout_comments = {v['name']: list(v['comments'].values()) for k, v in load_workouts.items() if 'comments' in v.keys()}
        for workout_name, comments in workout_comments.items():
            # loop over all comments per workout
            for comment in comments:
                # login the comment owner #TODO: group comments first to reduce logins
                owner = comment['owner_username']
                login(remote_browser, base_url, load_users[owner]['username'], load_users[owner]['password'])
                # redirect comment owner to workouts page (default tab "All Workouts" should display the given workout)
                remote_browser.get(urljoin(base_url, 'workouts.html'))
                # get list of displayed workouts and corresponding hrefs
                workouts_displayed, hrefs = get_displayed_workouts(remote_browser, base_url, '//*[@id="list-all-workouts-list"]')
                # click on given workout
                for w, h in dict(zip(workouts_displayed, hrefs)).items():
                    if w == workout_name:
                        # follow link to workout details page
                        remote_browser.get(h)
                        # find comment text area
                        remote_browser.find_element(by=By.ID, value='comment-area').send_keys(comment['content'])
                        # submit
                        click_element(remote_browser, '//*[@id="post-comment"]')
                        # check if click was successful

            # create_comments(remote_browser, base_url, workout_data['comments'])


@pytest.fixture(scope='session')
def base_url(request):
    """ :returns base_url based on --local option """
    local = request.config.getoption('--local')
    if request.config.getoption('--local') == 'true':
        # note: don't use localhost:9090 since this cannot be found from inside the selenium container
        base_url = 'http://host.docker.internal:9090'
    elif request.config.getoption('--local') == 'false':
        # TODO: replace by .env vars
        base_url = 'https://secfit-group32-frontend-dev.herokuapp.com'
    else:
        raise ValueError(f'--local={local}". Base_url could not be determined.')
    return base_url


@pytest.fixture(scope='session')
def remote_browser(test_browser, local) -> Remote:
    """ Select configuration depends on browser and host """
    if local != 'true' and local != 'false':
        raise ValueError(f'--local={local}". Driver could not be setup.\n'
                         'pass "true" if local execute\n'
                         'pass "false" if use CI service')
    cmd_executor = {
        'true': 'http://localhost:4444',
        'false': f'http://selenium__standalone-{test_browser}:4444'
    }
    if test_browser == 'firefox':
        driver = webdriver.Remote(
            options=webdriver.FirefoxOptions(),
            command_executor=cmd_executor[local])
    elif test_browser == 'chrome':
        options = webdriver.ChromeOptions()
        if local == 'true':
            options.add_experimental_option('excludeSwitches', ['enable-logging'])
            driver = webdriver.Chrome(options=options)
        else:
            driver = webdriver.Remote(
                options=options,
                command_executor=cmd_executor[local])
    else:
        raise ValueError(
            f'--browser="{test_browser}" is not chrome or firefox')
    yield driver
    driver.quit()
